const HtmlWebPackPlugin = require('html-webpack-plugin')
const { resolve } = require('path')
const fs = require('fs')

module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: /\.(css|scss)$/,
                use: [
                    { loader: 'style-loader' },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            localIdentName: '[local]__[hash:base64:5]',
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
                exclude: /node_modules/,
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
                include: /node_modules/,
            },
            {
                test: /.*?.svg$/,
                include: /src/,
                use: [
                    {
                        loader: '@svgr/webpack',
                        options: {
                            svgoConfig: {
                                plugins: {
                                    removeViewBox: false,
                                },
                            },
                        },
                    },
                ],
            },
            {
                test: /\.less$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader', // translates CSS into CommonJS
                    },
                    {
                        loader: 'less-loader', // compiles Less to CSS
                        options: {
                            modifyVars: {},
                            javascriptEnabled: true,
                        },
                    },
                ],
            },
        ],
    },
    resolve: {
        alias: {
            '#models': resolve(__dirname, 'src/models/'),
            '#remote': resolve(__dirname, 'src/remote/'),
            '#apps': resolve(__dirname, 'src/models/apps/'),
            '#components': resolve(__dirname, 'src/components/'),
            '#actions': resolve(__dirname, 'src/actions/'),
            '#utils': resolve(__dirname, 'src/utils/'),
            '#common': resolve(__dirname, 'src/common/'),
            '#logger': resolve(__dirname, 'src/logger/'),
        },
    },
    entry: {
        loader: './src/loader/',
        app: './src/',
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: './src/index.html',
            filename: './index.html',
            favicon: './favicon.ico',
        }),
    ],
}
