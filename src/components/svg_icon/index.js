import React from 'react'
import styles from './styles.css'

const SvgIcon = ({ name, pale, ...props }) => {
    const Svg = require('./svg/' + name + '.svg').default
    return (
        <Svg
            fill={pale ? 'lightgray' : 'gray'}
            width='1em'
            height='1em'
            className={styles.svg_icon}
            {...props}
        />
    )
}

export { SvgIcon }
