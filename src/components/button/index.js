import _ from 'lodash'
import React from 'react'
import clsx from 'clsx'

import { SvgIcon } from '../'
import { Button } from 'antd'
import styles from './styles.css'

_.assign(SvgIconButton, Button)

function SvgIconButton({ icon, pale, children, ...other }) {
    return (
        <Button {...other}>
            {icon && (
                <SvgIcon
                    name={icon}
                    pale={pale}
                    className={clsx(
                        styles.svg_icon,
                        children && styles.margin_right
                    )}
                />
            )}
            {children}
        </Button>
    )
}

export { SvgIconButton as Button }
