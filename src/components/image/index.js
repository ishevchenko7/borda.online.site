import _ from 'lodash'
import React from 'react'

const Image = ({ image, style, className }) => (
    <img
        style={style}        
        className={className}
        src={window.URL.createObjectURL(new Blob([image]))}
        onLoad={(event) => window.URL.revokeObjectURL(event.target.currentSrc)}
    />
)

export { Image }
