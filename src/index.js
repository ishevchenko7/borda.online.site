import React from 'react'
import { render } from 'react-dom'
import 'antd/dist/antd.css'

import Layout from '#models/layout'
import * as settingsStore from '#common/settings-store'

settingsStore.set({
    connectDialog: {
        maxAttemptsCount: 3,
        attemptDelay: 7000,
        connectionTimeout: 20000,
    },
})

const root = document.getElementById('root')
render(<Layout />, root)
