import * as settingsStore from '#common/settings-store'

export default async function (args) {
    const { settings } = args
    settingsStore.set(settings)
}
