import _ from 'lodash'

import * as settingsStore from '#common/settings-store'
import { RtcTransceiver, signalHandler } from '#common/signal-ready'
import appActionsDispatchersStore from '#remote/app_actions_dispatchers_store'

export default async function (args, dispatcher) {
    const { user, settings } = args

    const settingsPath = user
    if (settings) {
        settingsStore.set(settings, settingsPath)
    } else {
        settingsStore.copy('connection', settingsPath)
    }

    const transceiver = new RtcTransceiver({
        user,
        settingsPath,
        negotiate: true,
        signalSender: async (signal) =>
            await dispatcher.remoteCall({
                type: 'signal',
                args: { signal },
            }),
    })
    transceiver.addEventListener('close', () => {
        settingsStore.remove(settingsPath)
    })

    dispatcher.signalHandler = async (signal) =>
        await signalHandler({ transceiver, signal })

    await transceiver.readyState()

    appActionsDispatchersStore.add(user, {
        transceiver,
        settingsPath: `${settingsPath}.actionsDispatcher`,
    })

    dispatcher.readyEventResolver(user)
}
