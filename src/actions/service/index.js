import { ActionsDispatcher } from '#common/signal-ready'

const actions = require.context('./', true, /^\.\/(?!index(\.js)?$)/)

class ServiceActionsDispatcher extends ActionsDispatcher {
    constructor(params) {
        super({ actions, ...params })

        this.readyEventPromise = new Promise(
            (resolve) => (this.readyEventResolver = resolve)
        )
    }

    async readyState() {
        return await this.readyEventPromise
    }
}

export default ServiceActionsDispatcher
