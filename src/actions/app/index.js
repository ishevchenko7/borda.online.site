import { ActionsDispatcher } from '#common/signal-ready'

class AppActionsDispatcher extends ActionsDispatcher {
    constructor(params) {
        super(params)
    }
}

export default AppActionsDispatcher
