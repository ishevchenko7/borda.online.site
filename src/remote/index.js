import _ from 'lodash'
import url from 'url'
import parseDomain from 'parse-domain'
import { message } from 'antd'

import { SocketTransceiver } from '#common/signal-ready'
import { ActionsDispatchersStore } from '#common/signal-ready'
import withSettings from '#common/settings-store/with_settings'

import logger from '#logger'
import ServiceActionsDispatcher from '#actions/service'
import appActionsDispatchersStore from '#remote/app_actions_dispatchers_store'

class Remote {
    constructor() {
        withSettings(this, 'remote')

        this.board =
            process.env.NODE_ENV === 'development'
                ? process.env.BOARD_NAME
                : _.get(parseDomain(window.location.hostname), 'subdomain')

        this.appActionsDispatchersStore = new ActionsDispatchersStore({
            closeMarkFlag: true,
        })
        this.callCounter = 0
    }

    async restoreConnection(previousToken, appActionsDispatcher) {
        if (previousToken) {
            const { authenticated } = await appActionsDispatcher.remoteCall({
                type: 'authentication/restore',
                args: { dispatcherId: previousToken },
            })

            _.forEach(
                appActionsDispatchersStore.getIds({ closed: true }),
                (id) => {
                    const dispatcher = appActionsDispatchersStore.get({
                        id,
                    })

                    try {
                        if (
                            dispatcher.recallPendedActions(appActionsDispatcher)
                        ) {
                            appActionsDispatchersStore.remove(id)
                        }
                    } catch (error) {
                        logger.info('Recall pended action error', { error })
                    }
                }
            )

            return authenticated
        }
    }

    connect = async (closeHandler, connectionTimeout) => {
        return new Promise(async (resolve, reject) => {
            let socket = new WebSocket(
                url.format({
                    protocol: 'wss:',
                    hostname: 'borda.online',
                    pathname: '/ws/',
                    query: { source: 'site', board: this.board },
                })
            )

            socket.onopen = async () => {
                logger.info('WebSocket open')

                let timeoutTimer = setTimeout(function () {
                    socket.close()
                    socket = undefined
                    timeoutTimer = undefined

                    reject('Connection timeout')
                }, connectionTimeout)

                const serviceActionsDispatcher = new ServiceActionsDispatcher({
                    transceiver: new SocketTransceiver({ socket }),
                })

                const token = await serviceActionsDispatcher.readyState()

                if (timeoutTimer) {
                    clearTimeout(timeoutTimer)

                    const previousToken = this.token
                    this.token = token

                    this.appActionsDispatcher = appActionsDispatchersStore.get({
                        id: this.token,
                    })

                    const { transceiver } = this.appActionsDispatcher
                    transceiver.addEventListener('close', () => {
                        this.setConnectedState(false)
                        closeHandler()
                    })

                    const authenticated = await this.restoreConnection(
                        previousToken,
                        this.appActionsDispatcher
                    )

                    resolve({ authenticated })
                    this.setConnectedState(true)

                    logger.info('User connected')
                } else {
                    logger.warn('Connected after timeout')
                }
            }

            socket.onclose = ({ code }) => {
                logger.info(`WebSocket close code ${code}`)

                switch (code) {
                    case 4400:
                        reject('Board is offline')
                        break
                    case 4500:
                        reject('Internal error')
                        break
                }
            }

            socket.onerror = (error) => {
                logger.info({ error })
                reject('Service connection error')
            }
        })
    }

    call = async (action, remoteCallTimeout) => {
        let messageTimer
        const messageConfig = this.settings.longCallMessage[action.type] && {
            duration: 0,
            key: this.callCounter++,
            content: this.settings.longCallMessage[action.type],
        }

        try {
            if (this.connected) {
                if (messageConfig) {
                    messageTimer = setTimeout(() => {
                        message.loading(messageConfig)
                        messageTimer = undefined
                    }, this.settings.delayLongCallMessage)
                }

                const result = await this.appActionsDispatcher.remoteCall(
                    action,
                    remoteCallTimeout
                )

                if (result.warn) {
                    message.warn(result.warn)
                }

                return result
            } else {
                throw Error('Connection not established yet')
            }
        } catch (error) {
            message.error(`${error.name}: ${error.message}`)
            throw error
        } finally {
            if (messageConfig) {
                if (messageTimer) {
                    clearTimeout(messageTimer)
                } else {
                    message.destroy(messageConfig.key)
                }
            }
        }
    }

    addStream = async ({ video }) => {
        const { streamId } = await this.call({
            type: 'streams/add-stream',
            args: { video },
        })

        const { transceiver } = this.appActionsDispatcher
        return await transceiver.getRemoteStream(streamId)
    }

    isConnected = () => this.connected

    setConnectedState = async (connected) => {
        this.connected = connected
        if (connected && this.connectedStateResolver) {
            this.connectedStateResolver()
        }
    }

    connectedState = async () => {
        return new Promise((resolve) => {
            if (this.connected) {
                resolve()
            } else {
                this.connectedStateResolver = resolve
            }
        })
    }
}

export const {
    connect,
    call,
    addStream,
    connectedState,
    isConnected,
} = new Remote()
