import { ActionsDispatchersStore } from '#common/signal-ready'
import AppActionsDispatcher from '#actions/app'

class AppActionsDispatchersStore extends ActionsDispatchersStore {
    constructor() {
        super({ closeMarkFlag: true })
    }

    add(id, params) {
        const dispatcher = new AppActionsDispatcher(params)
        super.add(id, dispatcher)
    }
}

export default new AppActionsDispatchersStore()
