export function getUrlPath() {
    const match = /^(\/)(.*)$/g.exec(location.pathname)
    return match[2]
}
