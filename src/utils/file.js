import _ from 'lodash'
import pathParse from 'path-parse'

const supportedMediaTypes = {
    image: /(gif|jpg|jpeg|tiff|png|webp|avif|svg)/,
    audio: /(mp3)/,
}

export function getName(name = '') {
    return pathParse(name).name
}

export function getExtension(name) {
    return pathParse(name).ext.slice(1)
}

export function getMediaType(name) {
    const extension = getExtension(name)
    return (
        extension &&
        _.findKey(supportedMediaTypes, (filter) =>
            extension.toLowerCase().match(filter)
        )
    )
}

export function getFileKey({ dir, name }) {
    return `${dir}/${name}`
}
