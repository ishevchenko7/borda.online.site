import prettyMilliseconds from 'pretty-ms'

export function seconds2Time(timeInSeconds) {
    return prettyMilliseconds((timeInSeconds || 0) * 1000, {
        colonNotation: true,
        secondsDecimalDigits: 0,
    })
}
