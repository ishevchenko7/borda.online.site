import _ from 'lodash'
import { getName } from '#utils/file'

export function getTitle({ artist, title, fileName }) {
    return title ? _.compact([artist, title]).join(' - ') : getName(fileName)
}
