import _ from 'lodash'

import PureModel from '#common/viewmo'
import { call, addStream } from '#remote'

import ShareModal from '#apps/components/share_modal'
import render from './render'

const qualities = [
    { name: 'QVGA', quality: [320, 240] },
    { name: 'VGA', quality: [640, 480] },
    { name: 'HD', quality: [1280, 720] },
    { name: 'Full HD', quality: [1920, 1080] },
]

const initialQuality = 'VGA'

class CctvApp extends PureModel {
    constructor(params) {
        super({ render })

        const shareModal = new ShareModal()
        _.assign(this, {
            ...params,
            qualities,
            shareModal,
        })

        if (!_.isEmpty(this.webcams)) {
            const { deviceId } = _.first(this.webcams)
            this.initialValues = { deviceId, quality: initialQuality }
        }
    }

    async closeCurrentStream() {
        const stream = this.videoRef.srcObject
        if (stream) {
            this.videoRef.srcObject = undefined

            await Promise.all(
                _.map(stream.getTracks(), async (track) => {
                    track.stop()
                    return await call({
                        type: 'streams/remove-track',
                        args: { trackId: track.id },
                    })
                })
            )
        }
    }

    onFinish = async (values) => {
        this.showStream(values)
    }

    shareClick = async () => {
        const { deviceId } = await this.formRef.validateFields()
        const { label } = _.find(this.webcams, { deviceId })

        const items = [
            {
                name: deviceId,
                alias: label,
                type: 'webcam',
                anonymous: false,
                url_path: null,
            },
        ]

        this.shareModal.show({
            items,
        })
    }

    async showStream({ deviceId, quality }) {
        await this.closeCurrentStream()

        const [width, height] = _.find(
            qualities,
            ({ name }) => quality === name
        ).quality

        const video = {
            deviceId,
            width: { ideal: width },
            height: { ideal: height },
        }

        const stream = await addStream({ video })
        this.videoRef.srcObject = stream
    }

    setVideoRef = (ref) => {
        this.videoRef = ref
    }

    setFormRef = (ref) => {
        this.formRef = ref
    }

    async onMount() {
        if (_.isEmpty(this.webcams)) {
            const webcams = await call({
                type: 'apps/cctv/webcams',
            })

            if (!_.isEmpty(webcams)) {
                const { deviceId } = _.first(webcams)

                this.update({
                    initialValues: { deviceId, quality: initialQuality },
                    webcams: _.map(webcams, ({ deviceId, label }) => ({
                        deviceId,
                        label,
                    })),
                })
            }
        }

        if (!_.isEmpty(this.webcams)) {
            this.showStream(this.formRef.getFieldsValue())
        }
    }

    async onUnmount() {
        this.closeCurrentStream()
    }
}

export default CctvApp
