import _ from 'lodash'
import React from 'react'
import { Select, Form } from 'antd'

import { Button } from '#components'
import styles from './styles.scss'

const { Option } = Select

export default function () {
    const { shareModal } = this

    return (
        <div className={styles.container}>
            <div>
                {!_.isEmpty(this.webcams) && (
                    <>
                        <Form
                            ref={this.setFormRef}
                            onFinish={this.onFinish}
                            layout='inline'
                            initialValues={this.initialValues}
                            className={styles.panel}
                        >
                            <Form.Item
                                name='deviceId'
                                rules={[
                                    {
                                        required: true,
                                    },
                                ]}
                            >
                                <Select
                                    size='large'
                                    placeholder='deviceId'
                                    style={{ width: 200 }}
                                    defaultActiveFirstOption
                                >
                                    {_.map(
                                        this.webcams,
                                        ({ deviceId, label }) => (
                                            <Option key={deviceId}>
                                                {label}
                                            </Option>
                                        )
                                    )}
                                </Select>
                            </Form.Item>
                            <Form.Item
                                name='quality'
                                rules={[
                                    {
                                        required: true,
                                    },
                                ]}
                            >
                                <Select size='large' style={{ width: 100 }}>
                                    {_.map(this.qualities, ({ name }) => (
                                        <Option key={name} value={name}>
                                            {name}
                                        </Option>
                                    ))}
                                </Select>
                            </Form.Item>

                            <Form.Item>
                                <Button.Group size='large'>
                                    <Button htmlType='submit' icon='play' />
                                    <Button
                                        icon='share'
                                        onClick={this.shareClick}
                                    />
                                </Button.Group>
                            </Form.Item>
                        </Form>
                    </>
                )}
            </div>
            <div className={styles.video}>
                <video muted autoPlay ref={this.setVideoRef} />
            </div>
            <shareModal.View />
        </div>
    )
}
