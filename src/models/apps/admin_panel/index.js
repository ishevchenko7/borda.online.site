import _ from 'lodash'
import PureModel from '#common/viewmo'

import * as panels from './panels'
import render from './render'

const defaultPanel = 'Users'

class AdminPanel extends PureModel {
    constructor() {
        super({ render })

        _.assign(this, {
            activePanel: defaultPanel,
            panel: PureModel.stub,
        })
    }

    createPanel(activePanel) {
        const Panel = panels[activePanel]
        return new Panel()
    }

    showPanel(panel) {
        this.update({
            activePanel: panel,
            panel: this.createPanel(panel),
        })
    }

    handleMenuClick = (e) => {
        this.showPanel(e.key)
    }

    onMount() {
        this.showPanel(defaultPanel)
    }
}

export default AdminPanel
