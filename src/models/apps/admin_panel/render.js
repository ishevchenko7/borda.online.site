import _ from 'lodash'
import React from 'react'
import { Menu } from 'antd'

import styles from './styles.scss'

export default function () {
    const { panel } = this

    return (
        <div className={styles.container}>
            <div>
                <Menu
                    selectedKeys={this.activePanel}
                    onClick={this.handleMenuClick}
                    mode='horizontal'
                >
                    <Menu.Item key='Users'>Users</Menu.Item>
                    <Menu.Item key='Shared'>Shared</Menu.Item>
                </Menu>
            </div>
            <div>
                <panel.View />
            </div>
        </div>
    )
}
