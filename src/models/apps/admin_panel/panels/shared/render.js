import _ from 'lodash'
import React from 'react'
import styles from './styles.scss'

export default function () {
    const { sharedPanel } = this

    return (
        <div className={styles.container}>
            <sharedPanel.View />
        </div>
    )
}
