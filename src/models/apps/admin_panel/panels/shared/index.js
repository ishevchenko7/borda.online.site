import _ from 'lodash'

import PureModel from '#common/viewmo'
import SharedTable from './shared_table'

import render from './render'

class SharedPanel extends PureModel {
    constructor() {
        super({ type: 'AdminPanel/SharedPanel', render })

        _.assign(this, {
            sharedPanel: new SharedTable(),
        })
    }
}

export default SharedPanel
