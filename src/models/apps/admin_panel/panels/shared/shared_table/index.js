import _ from 'lodash'
import React from 'react'
import { Menu, Dropdown, Tag } from 'antd'

import logger from '#logger'
import { call } from '#remote'
import PureModel from '#common/viewmo'

import SharedManageModal from '#apps/components/shared_manage_modal'
import styles from './styles.scss'

import render from './render'

class SharedTable extends PureModel {
    constructor(params) {
        super({ type: 'AdminPanel/SharedTable', render })

        _.assign(this, {
            sharedManageModal: new SharedManageModal({
                parent: this.sharedManageModalHandlers,
            }),
            filters: {},
            ...params,
        })

        this.data = []
        this.columns = [
            {
                title: 'Type',
                dataIndex: 'type',
                width: '12%',
                sorter: (a, b) =>
                    this.convertType(a.type).localeCompare(
                        this.convertType(b.type)
                    ),
                filters: [
                    { text: 'Folder', value: 'dir' },
                    { text: 'Webcam', value: 'webcam' },
                    { text: 'File', value: 'file' },
                ],
                render: (text) => (
                    <Tag>{this.convertType(text).toUpperCase()}</Tag>
                ),
            },
            {
                title: 'Alias',
                dataIndex: 'alias',
                required: true,
                editable: true,
                sorter: (a, b) => a.alias.localeCompare(b.alias),
                ellipsis: true,
            },
            {
                title: 'Anonymous',
                dataIndex: 'anonymous',
                type: 'boolean',
                width: '15%',
                editable: true,
                sorter: (a, b) => b.anonymous - a.anonymous,
                ellipsis: true,
            },
            {
                title: 'URL',
                dataIndex: 'url_path',
                width: '10%',
                sorter: (a, b) => Boolean(b.url_path) - Boolean(a.url_path),
                render: (value) => (
                    <div className={styles.url_enabled_cell}>
                        <Tag color={Boolean(value) && 'green'}>
                            {value ? 'Yes' : 'No'}
                        </Tag>
                    </div>
                ),
            },
            {
                title: '',
                dataIndex: 'actions',
                width: '12%',
                render: (text, record) => {
                    const menu = (
                        <Menu>
                            <Menu.Item key='edit'>
                                <a onClick={() => this.edit(record)}>Edit</a>
                            </Menu.Item>
                            <Menu.Item key='remove'>
                                <a onClick={() => this.remove(record)}>
                                    Remove
                                </a>
                            </Menu.Item>
                            <Menu.Item key='manage'>
                                <a onClick={() => this.shareManage(record)}>
                                    Manage
                                </a>
                            </Menu.Item>
                        </Menu>
                    )

                    return this.isEditing(record) ? (
                        <span className={styles.actions}>
                            <a onClick={() => this.save(record)}>Save</a>
                            <br></br>
                            <a onClick={() => this.cancel()}>Cancel</a>
                        </span>
                    ) : (
                        <Menu>
                            <Dropdown
                                overlay={menu}
                                trigger={['click']}
                                disabled={Boolean(this.editingId)}
                            >
                                <a onClick={(e) => e.preventDefault()}>
                                    actions&nbsp;⌄
                                </a>
                            </Dropdown>
                        </Menu>
                    )
                },
            },
        ]

        this.mergedColumns = this.columns.map((column) => {
            if (!column.editable) {
                return column
            }

            return {
                ...column,
                onCell: (record, index) => {
                    const value = record[column.dataIndex]
                    return {
                        record,
                        index,
                        value,
                        type: column.type,
                        required: column.required,
                        dataIndex: column.dataIndex,
                        editing: this.isEditing(record),
                    }
                },
            }
        })
    }

    convertType(type) {
        return type === 'dir' ? 'folder' : type
    }

    shareManage(record) {
        this.sharedManageModal.show(record)
    }

    isEditing(record) {
        return record.id === this.editingId
    }

    setEditingId(editingId) {
        this.update({
            editingId,
        })
    }

    edit(record) {
        this.formRef.setFieldsValue({
            name: '',
            alias: '',
            anonymous: false,
            disabled: false,
            ...record,
        })

        this.setEditingId(record.id)
    }

    async save(old) {
        let validated
        try {
            validated = await this.formRef.validateFields()
        } catch (error) {
            logger.error('Fields validation failed:', error)
            return
        }

        await call({
            type: 'apps/admin-panel/shared/update',
            args: {
                id: old.id,
                value: validated,
            },
        })

        this.updateList()
    }

    async remove({ id }) {
        await call({
            type: 'apps/admin-panel/shared/remove',
            args: {
                id,
            },
        })

        this.updateList()
    }

    cancel = () => {
        this.setEditingId()
    }

    async updateList() {
        const data = await call({
            type: 'apps/admin-panel/shared/get',
            args: {
                types: this.filters.type || [],
            },
        })

        this.update({
            data,
            editingId: undefined,
        })
    }

    onTableChange = (pagination, filters) => {
        const filtersChanged = !_.isEqual(filters, this.filters)
        if (filtersChanged) {
            this.filters = filters
            this.updateList()
        }
    }

    setFormRef = (ref) => {
        this.formRef = ref
    }

    sharedManageModalHandlers = {
        onConfirm: async ({ id, ...fields }) => {
            await call({
                type: 'apps/admin-panel/shared/update',
                args: {
                    id,
                    value: fields,
                },
            })

            this.updateList()
        },
    }

    async onMount() {
        this.updateList()
    }
}

export default SharedTable
