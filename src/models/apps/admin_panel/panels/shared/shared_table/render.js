import _ from 'lodash'
import React from 'react'
import { Form, Table, Input, Checkbox } from 'antd'

import styles from './styles.scss'

const EditableCell = ({
    record,
    index,
    value,
    type,
    dataIndex,
    required,
    editing,
    children,
    ...restProps
}) => {
    const common = {
        name: dataIndex,
        style: {
            margin: 0,
        },
        rules: [
            {
                required,
                message: 'Field is mandatory!',
            },
        ],
    }

    const Cell = () => {
        switch (type) {
            case 'boolean':
                return editing ? (
                    <Form.Item {...common} valuePropName='checked'>
                        <Checkbox defaultChecked={value} />
                    </Form.Item>
                ) : (
                    <Checkbox defaultChecked={value} disabled={true} />
                )

            default:
                return editing ? (
                    <Form.Item {...common}>
                        <Input />
                    </Form.Item>
                ) : (
                    children
                )
        }
    }

    return (
        <td {...restProps}>
            <Cell />
        </td>
    )
}

export default function () {
    const { sharedManageModal } = this
    
    return (
        <div className={styles.container}>
            <Form ref={this.setFormRef} component={false}>
                <Table
                    rowKey='id'
                    scroll={{ x: 800 }}
                    className={styles.table}
                    rowClassName={styles.editable_row}
                    pagination={false}
                    onChange={this.onTableChange}
                    components={{
                        body: {
                            cell: EditableCell,
                        },
                    }}
                    bordered
                    dataSource={this.data}
                    columns={this.mergedColumns}
                    expandable={{
                        expandedRowRender: (record) => (
                            <p style={{ margin: 0 }}>{record.name}</p>
                        ),
                        rowExpandable: () => true,
                    }}
                    onRow={(record, index) => ({
                        index,
                    })}
                />
            </Form>
            <sharedManageModal.View />
        </div>
    )
}
