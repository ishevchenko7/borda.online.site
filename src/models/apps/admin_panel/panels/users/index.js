import _ from 'lodash'
import React from 'react'
import { Menu, Dropdown, Tag } from 'antd'

import PureModel from '#common/viewmo'

import logger from '#logger'
import { call } from '#remote'
import styles from './styles.scss'
import SetPasswordModal from './set_password_modal'

import render from './render'

class UsersPanel extends PureModel {
    constructor() {
        super({ type: 'AdminPanel/UsersPanel', render })

        this.setPasswordModal = new SetPasswordModal({
            parent: this.setPasswordModalHandlers,
        })

        this.data = []
        this.columns = [
            {
                title: 'Full Name',
                dataIndex: 'full_name',
                editable: true,
            },
            {
                title: 'Name',
                dataIndex: 'name',
                editable: true,
                required: true,
                width: '27%',
            },
            {
                title: 'Password Set',
                dataIndex: 'password',
                editable: false,
                width: '20%',
                render: (value) => (
                    <div className={styles.password_cel}>
                        <Tag color={value ? 'green' : 'red'}>
                            {value ? 'Yes' : 'No'}
                        </Tag>
                    </div>
                ),
            },
            {
                title: '',
                dataIndex: 'actions',
                width: '15%',
                render: (text, record) => {
                    const menu = (
                        <Menu>
                            <Menu.Item key='edit'>
                                <a onClick={() => this.edit(record)}>Edit</a>
                            </Menu.Item>
                            <Menu.Item key='remove'>
                                <a onClick={() => this.remove(record)}>
                                    Remove
                                </a>
                            </Menu.Item>
                            <Menu.Item key='password'>
                                <a onClick={() => this.setPassword(record)}>
                                    {`${
                                        record.password ? 'Reset' : 'Set'
                                    } Password`}
                                </a>
                            </Menu.Item>
                        </Menu>
                    )

                    const editable = this.isEditing(record)
                    return editable ? (
                        <span className={styles.actions}>
                            <a onClick={() => this.save(record)}>Save</a>
                            <a onClick={() => this.cancel(record)}>Cancel</a>
                        </span>
                    ) : (
                        <Menu>
                            <Dropdown
                                overlay={menu}
                                trigger={['click']}
                                disabled={this.editingName !== ''}
                            >
                                <a onClick={(e) => e.preventDefault()}>
                                    actions&nbsp;⌄
                                </a>
                            </Dropdown>
                        </Menu>
                    )
                },
            },
        ]

        this.mergedColumns = this.columns.map((column) => {
            if (!column.editable) {
                return column
            }

            return {
                ...column,
                onCell: (record, index) => {
                    return {
                        record,
                        index,
                        dataIndex: column.dataIndex,
                        title: column.title,
                        required: column.required,
                        editing: this.isEditing(record),
                    }
                },
            }
        })
    }

    isEditing(record) {
        return record.name === this.editingName
    }

    setEditingName(editingName) {
        this.update({
            editingName,
        })
    }

    edit(record) {
        this.formRef.setFieldsValue({
            name: '',
            full_name: '',
            ...record,
        })

        this.setEditingName(record.name)
    }

    setFormRef = (ref) => {
        this.formRef = ref
    }

    async save(old) {
        let validated
        try {
            validated = await this.formRef.validateFields()
        } catch (error) {
            logger.error('Validate Failed:', error)
            return
        }

        const user = _.find(this.data, { name: validated.name })
        if (!user || user === old) {
            validated = _.pick(validated, ['name', 'full_name'])
            if (old.name) {
                await call({
                    type: 'apps/admin-panel/user/update',
                    args: {
                        name: old.name,
                        user: validated,
                    },
                })
            } else {
                await call({
                    type: 'apps/admin-panel/user/add',
                    args: {
                        ...validated,
                    },
                })
            }

            this.updateList()
        } else {
            this.formRef.setFields([
                {
                    name: 'name',
                    errors: ['Uniq name required'],
                },
            ])
        }
    }

    async updateList(data) {
        if (!data) {
            data = await call({
                type: 'apps/admin-panel/user/get',
            })
        }

        this.update({
            data,
            editingName: '',
        })
    }

    setPassword(user) {
        this.setPasswordModal.show(user)
    }

    async remove({ name }) {
        await call({
            type: 'apps/admin-panel/user/remove',
            args: {
                name,
            },
        })

        this.updateList()
    }

    cancel = (record) => {
        if (record.name === '') {
            const data = _.without(this.data, record)
            this.updateList(data)
        } else {
            this.setEditingName('')
        }
    }

    onAddClick = () => {
        const record = {
            name: '',
            full_name: '',
        }

        this.update({ data: [...this.data, record] })
        this.edit(record)
    }

    setPasswordModalHandlers = {
        onConfirm: async ({ name, password }) => {
            try {
                await call({
                    type: 'apps/admin-panel/user/set-password',
                    args: {
                        name,
                        password,
                    },
                })

                this.updateList()
            } finally {
                this.setPasswordModal.hide()
            }
        },
    }

    async onMount() {
        this.updateList()
    }
}

export default UsersPanel
