import _ from 'lodash'
import React from 'react'
import { Table, Input, Form, Button } from 'antd'

import styles from './styles.scss'

const EditableCell = ({
    record,
    editing,
    dataIndex,
    required,
    title,
    index,
    children,
    ...restProps
}) => {
    return (
        <td {...restProps}>
            {editing ? (
                <Form.Item
                    name={dataIndex}
                    style={{
                        margin: 0,
                    }}
                    rules={[
                        {
                            required,
                            message: 'Field is mandatory!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
            ) : (
                children
            )}
        </td>
    )
}

export default function () {
    const { setPasswordModal } = this
    return (
        <div className={styles.container}>
            <div>
                <Button value='large' type='link' onClick={this.onAddClick}>
                    Create
                </Button>
            </div>
            <div className={styles.form}>
                <Form ref={this.setFormRef} component={false}>
                    <Table
                        rowKey='name'
                        rowClassName={styles.editable_row}
                        pagination={false}
                        components={{
                            body: {
                                cell: EditableCell,
                            },
                        }}
                        bordered
                        dataSource={this.data}
                        columns={this.mergedColumns}
                        onRow={(record, index) => ({
                            index,
                        })}
                    />
                </Form>
            </div>
            <setPasswordModal.View />
        </div>
    )
}