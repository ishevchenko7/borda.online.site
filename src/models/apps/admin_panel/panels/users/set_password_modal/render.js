import React from 'react'
import { Button, Modal, Form, Input } from 'antd'

import styles from './styles.scss'

export default function () {
    return (
        <Modal
            className={styles.container}
            visible={this.visible}
            onCancel={this.onCancel}
            title={`Password for ${this.name}`}
            footer={[
                <Button
                    key='ok'
                    type='primary'
                    onClick={this.onOk}
                    loading={this.pending}
                >
                    OK
                </Button>,
                <Button key='cancel' type='primary' onClick={this.onCancel}>
                    Cancel
                </Button>,
            ]}
        >
            <Form layout='vertical' name='confirmation' ref={this.setFormRef}>
                <Form.Item
                    name='password'
                    label='Password'
                    rules={[
                        {
                            required: true,
                            message: 'Input password',
                        },
                    ]}
                    hasFeedback
                >
                    <Input.Password autoComplete='new-password' />
                </Form.Item>

                <Form.Item
                    name='confirm'
                    label='Confirm Password'
                    dependencies={['password']}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: 'Confirm password',
                        },
                        ({ getFieldValue }) => ({
                            validator(rule, value) {
                                if (
                                    !value ||
                                    getFieldValue('password') === value
                                ) {
                                    return Promise.resolve()
                                }
                                return Promise.reject(
                                    'Password and confirmation do not match'
                                )
                            },
                        }),
                    ]}
                >
                    <Input.Password autoComplete='new-password' />
                </Form.Item>
            </Form>
        </Modal>
    )
}
