import _ from 'lodash'
import PureModel from '#common/viewmo'

import render from './render'

class SetPasswordModal extends PureModel {
    constructor(params) {
        super({ render })
        _.assign(this, { ...params })
    }

    show = ({ name }) => {
        if (this.formRef) {
            this.formRef.resetFields()
        }

        this.update({
            name,
            pending: false,
            visible: true,
        })
    }

    hide = () => {
        this.update({
            visible: false,
        })
    }

    setFormRef = (ref) => {
        this.formRef = ref
    }

    onOk = async () => {
        const { password } = await this.formRef.validateFields()

        this.update({ pending: true })
        this.parent.onConfirm({ name: this.name, password })
    }

    onCancel = () => {
        this.hide()
    }
}

export default SetPasswordModal
