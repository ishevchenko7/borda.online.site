import _ from 'lodash'
import pEvent from 'p-event'
import { message } from 'antd'

import { call, isConnected } from '#remote'
import PureModel from '#common/viewmo'
import withSettings from '#common/settings-store/with_settings'
import render from './render'

// TODO const!
const defaultStepSeekTime = 10

class Player extends PureModel {
    constructor(params) {
        super({ type: 'Audio/Player', render })
        _.assign(this, { ...params, bufferedBytes: 0 })

        withSettings(this, 'audioPlayer')
        this.initMediaSession()
    }

    initMediaSession() {
        if ('mediaSession' in navigator) {
            navigator.mediaSession.setActionHandler('seekto', (details) => {
                if (details.fastSeek && 'fastSeek' in this.audioRef) {
                    this.audioRef.fastSeek(details.seekTime)
                } else {
                    this.audioRef.currentTime = details.seekTime
                }
            })

            navigator.mediaSession.setActionHandler(
                'seekbackward',
                (details) => {
                    const skipTime = details.seekOffset || defaultStepSeekTime
                    this.audioRef.currentTime = Math.max(
                        this.audioRef.currentTime - skipTime,
                        0
                    )
                }
            )

            navigator.mediaSession.setActionHandler(
                'seekforward',
                (details) => {
                    const skipTime = details.seekOffset || defaultStepSeekTime
                    this.audioRef.currentTime = Math.min(
                        this.audioRef.duration,
                        this.audioRef.currentTime + skipTime
                    )
                }
            )

            navigator.mediaSession.setActionHandler('play', async () => {
                await this.play()
                navigator.mediaSession.playbackState = 'playing'
            })

            navigator.mediaSession.setActionHandler('pause', () => {
                this.pause()
                navigator.mediaSession.playbackState = 'paused'
            })
        }
    }

    async initMediaSource() {
        this.mediaSource = new MediaSource()
        this.audioRef.src = window.URL.createObjectURL(this.mediaSource)

        await this.sourceOpenState()
        this.mediaSource.duration = this.duration
        this.sourceBuffer = this.mediaSource.addSourceBuffer('audio/mpeg')

        await this.updateSourceBuffer()
    }

    updatePositionState() {
        navigator.mediaSession.setPositionState({
            duration: this.duration || 0.0,
            position: this.audioRef.currentTime || 0.0,
        })
    }

    get paused() {
        return this.audioRef.paused
    }

    playToggle = async () => {
        if (this.paused) {
            await this.play()
        } else {
            this.pause()
        }
    }

    async play() {
        if (this.audioRef.paused) {
            try {
                await this.audioRef.play()
            } catch {
                message.warn('Autoplay was prevented by browser')
                this.parent.pause()
            }
        }
    }

    pause() {
        this.audioRef.pause()
    }

    setVolume = (volume) => {
        this.audioRef.volume = volume / 100
    }

    setCurrentTime = (time) => {
        this.audioRef.currentTime = time
    }

    onSeeking = async () => {
        await this.updateSourceBuffer()
    }

    onTimeUpdate = async () => {
        this.parent.timeUpdate(this.audioRef.currentTime)
        if (isConnected()) {
            await this.updateSourceBuffer()
        }
    }

    onProgress = () => {
        if (this.audioRef.buffered.length > 0) {
            this.parent.progress(this.audioRef.buffered.end(0))
        }
    }

    onEnded = () => {
        this.parent.ended()
    }

    onPause = () => {
        this.stopUpdating()
        this.parent.pause()
    }

    onPlay = () => {
        this.parent.play()
    }

    setAudioRef = async (ref) => {
        if (ref) {
            this.audioRef = ref
            await this.initMediaSource()
        } else {
            await this.stopUpdating()

            window.URL.revokeObjectURL(this.audioRef.src)
            this.audioRef.src = undefined
            this.audioRef = undefined
            navigator.mediaSession.setPositionState(null)
        }
    }

    async updateEndState() {
        if (this.sourceBuffer.updating) {
            await pEvent(this.sourceBuffer, 'updateend')
        }
    }

    async sourceOpenState() {
        if (this.mediaSource.readyState !== 'open') {
            await pEvent(this.mediaSource, 'sourceopen')
        }
    }

    async unloadStep(step) {
        if (step.loaded) {
            const { time, length, duration } = step

            await this.updateEndState()
            this.sourceBuffer.remove(time, time + duration)

            step.loaded = false
            this.bufferedBytes -= length
        }
    }

    async loadStep(i) {
        const step = this.steps[i]

        if (!step.loaded) {
            const { time, position, length } = step

            const { bytesRead, data } = await call({
                type: 'apps/audio/read-file',
                args: {
                    file: this.file,
                    position,
                    length,
                },
            })

            if (bytesRead === length) {
                await this.updateEndState()
                this.sourceBuffer.timestampOffset = time
                this.sourceBuffer.appendBuffer(data)

                step.loaded = true
                this.bufferedBytes += length
            }
        }

        // Check if last step
        if (
            this.mediaSource.readyState === 'open' &&
            i === this.steps.length - 2
        ) {
            await this.updateEndState()
            this.mediaSource.endOfStream()
        }
    }

    needStopUpdate() {
        if (this.stopUpdatingResolver) {
            this.stopUpdatingResolver()
            return true
        }
    }

    // Waiting for stop previous update buffer call
    async stopUpdating() {
        await new Promise((resolve) => {
            if (this.updating) {
                this.stopUpdatingResolver = resolve
            } else {
                resolve()
            }
        })

        delete this.stopUpdatingResolver
    }

    async updateSourceBuffer() {
        await this.stopUpdating()

        try {
            this.updating = true
            let alreadyPlaying = false

            const currentStepIndex =
                this.audioRef.currentTime > 0
                    ? _.findIndex(
                          this.steps,
                          ({ time }) => time >= this.audioRef.currentTime
                      ) - 1
                    : 0

            if (
                currentStepIndex !== this.currentStepIndex ||
                this.audioRef.currentTime === 0
            ) {
                this.currentStepIndex = currentStepIndex

                if (this.settings.maxBufferedBytes < this.payloadLength) {
                    for (let i = 0; i < currentStepIndex; i++) {
                        await this.unloadStep(this.steps[i])
                        if (this.needStopUpdate()) {
                            return
                        }
                    }
                }

                let i = currentStepIndex
                while (i < this.steps.length - 1) {
                    const step = this.steps[i]

                    if (
                        this.bufferedBytes + step.length <=
                        this.settings.maxBufferedBytes
                    ) {
                        await this.loadStep(i++)

                        if (this.needStopUpdate()) {
                            return
                        }

                        if (!alreadyPlaying) {
                            this.play()
                            alreadyPlaying = true
                        }
                    } else {
                        const last = _.findLastIndex(
                            this.steps,
                            (step) => step.loaded
                        )
                        if (last <= i) {
                            break
                        }

                        await this.unloadStep(this.steps[last])
                        if (this.needStopUpdate()) {
                            return
                        }
                    }
                }
            }
        } finally {
            this.updating = false
        }
    }
}

export default Player
