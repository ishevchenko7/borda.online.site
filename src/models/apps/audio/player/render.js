import _ from 'lodash'
import React from 'react'

export default function () {
    return (
        <>
            <audio
                src={this.src}
                volume={this.volume}
                ref={this.setAudioRef}
                onSeeking={this.onSeeking}
                onTimeUpdate={_.throttle(this.onTimeUpdate, 1000, {
                    trailing: false,
                })}
                onProgress={this.onProgress}
                onEnded={this.onEnded}
                onPause={this.onPause}
                onPlay={this.onPlay}
            />
        </>
    )
}
