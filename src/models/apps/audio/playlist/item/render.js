import _ from 'lodash'
import React from 'react'
import clsx from 'clsx'

import { Checkbox } from 'antd'

import { getTitle } from '#utils/mp3'
import { seconds2Time } from '#utils/time'

import styles from './styles.scss'

export default function () {
    const { title = '', artist = '', duration } = this.meta || {}
    return (
        <div
            style={{ height: this.height }}
            className={clsx(styles.container, this.active && styles.active)}
        >
            <div className='checkbox'>
                <Checkbox
                    onChange={this.onCheckedChange}
                    checked={this.checked}
                />
                <span className={styles.title} onClick={this.onClick}>
                    {getTitle({ title, artist, fileName: this.file.name })}
                </span>
            </div>
            {duration && <div>{seconds2Time(duration)}</div>}
        </div>
    )
}
