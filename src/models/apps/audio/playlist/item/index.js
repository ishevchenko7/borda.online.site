import _ from 'lodash'

import PureModel from '#common/viewmo'
import render from './render'

class Item extends PureModel {
    constructor(params) {
        super({ type: 'Audio/Playlist/Item', render })
        _.assign(this, { ...params, height: 24 })
    }

    check = () => {
        this.update({ checked: !this.checked })
        return this.checked
    }

    updateActive = (active) => {
        this.update({ active })
    }

    updateMeta = (meta) => {
        this.update({ meta })
    }

    onClick = () => {
        this.parent.doubleClick(this)
    }

    onCheckedChange = ({ target: { checked } }) => {
        this.update({ checked })
        this.parent.checkedChange(this, checked)
    }
}

export default Item
