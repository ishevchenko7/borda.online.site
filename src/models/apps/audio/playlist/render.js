import _ from 'lodash'
import React from 'react'
import { List, AutoSizer } from 'react-virtualized'

import { Button } from '#components'
import styles from './styles.scss'

export default function () {
    return (
        <div className={styles.playlist}>
            <div key={this.updateCounter} className={styles.items}>
                <AutoSizer>
                    {({ width, height }) => (
                        <List
                            ref={this.setItemsRef}
                            className={styles.list}
                            height={height}
                            width={width}
                            scrollToAlignment='center'
                            overscanRowCount={10}
                            rowCount={this.items.length}
                            rowHeight={this.getItemHeight}
                            rowRenderer={this.rowRenderer}
                            onRowsRendered={this.onRowsRendered}
                            scrollToIndex={this.scrollToIndex}
                        />
                    )}
                </AutoSizer>
            </div>
            <div className={styles.footer}>
                <div className={styles.buttons}>
                    <Button.Group size='large'>
                        <Button icon='up-arrow' onClick={this.onUpArrowClick} />
                        <Button
                            icon='down-arrow'
                            onClick={this.onDownArrowClick}
                        />
                        <Button icon='check' onClick={this.onCheckClick} />
                        <Button icon='eraser' onClick={this.onCleanClick} />
                    </Button.Group>
                    <Button.Group size='large'>
                        <Button icon='random' onClick={this.onRandomClick} />
                    </Button.Group>
                </div>
                <div className={styles.counters}>
                    {`${this.checkedCounter}/${this.items.length}`}
                </div>
            </div>
        </div>
    )
}
