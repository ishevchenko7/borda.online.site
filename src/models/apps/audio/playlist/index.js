import _ from 'lodash'
import React from 'react'
import { message } from 'antd'

import { getFileKey } from '#utils/file'
import { call, connectedState } from '#remote'
import PureModel from '#common/viewmo'

import Item from './item'
import render from './render'

class Playlist extends PureModel {
    constructor(params) {
        super({ type: 'Audio/Playlist', render })

        _.assign(this, {
            ...params,
            items: [],
            metas: {},
            itemsCounter: 0,
            updateCounter: 0,
            checkedCounter: 0,
        })

        _.forEach(this.files, (file) => this.appendFile(file))
    }

    appendFile(file) {
        const number = this.itemsCounter++
        const meta = this.metas[getFileKey(file)]
        const item = new Item({
            file,
            meta,
            number,
            checked: false,
            parent: this.itemHandlers,
        })

        this.items.push(item)
        return item
    }

    append({ files, activate }) {
        _.forEach(files, (file) => {
            const item = this.appendFile(file)
            if (activate) {
                activate = false
                this.setActiveItem({ item })
            }
        })

        this.update()
        message.success(`${files.length} tracks added to audio playlist`)
    }

    getItemHeight = ({ index }) => {
        return this.items[index].height
    }

    getChecked() {
        const checked = _.filter(this.items, { checked: true })
        return _.isEmpty(checked) ? undefined : checked
    }

    sortUpdate(scrollToIndex) {
        this.update({
            scrollToIndex,
            updateCounter: ++this.updateCounter,
            items: _.sortBy(this.items, ['number']),
        })
    }

    recountItems() {
        if (!_.isEmpty(this.items)) {
            for (let i = 0; i < this.items.length; i++) {
                this.items[i].number = i
            }
        }
    }

    rowRenderer = ({ index, key, style }) => {
        const item = this.items[index]
        return (
            <div key={key} style={style}>
                <item.View />
            </div>
        )
    }

    onUpArrowClick = () => {
        const checked = this.getChecked()

        if (checked) {
            const { number: head } = _.head(checked)

            if (head > 0) {
                let checkedCounter = 0
                const checkedCount = checked.length

                _.forEach(this.items, (item) => {
                    if (item.checked) {
                        item.number = head + checkedCounter++ - 1
                    } else {
                        if (item.number >= head - 1) {
                            item.number += checkedCount - checkedCounter
                        }
                    }
                })

                this.sortUpdate(head)
            }
        }
    }

    onDownArrowClick = () => {
        const checked = this.getChecked()

        if (checked) {
            const { number: last } = _.last(checked)

            if (last < this.items.length - 1) {
                let checkedCounter = 0
                const checkedCount = checked.length
                const { number: head } = _.head(checked)

                _.forEachRight(this.items, (item) => {
                    if (item.checked) {
                        item.number = last - checkedCounter++ + 1
                    } else {
                        if (item.number <= last + 1 && item.number > head) {
                            item.number -= checkedCount - checkedCounter
                        }
                    }
                })

                this.sortUpdate(head)
            }
        }
    }

    onCheckClick = () => {
        let checkedCounter = 0
        _.forEach(this.items, (item) => {
            if (item.check()) {
                checkedCounter++
            }
        })

        this.update({ checkedCounter })
    }

    onCleanClick = () => {
        this.items = _.filter(this.items, { checked: false })

        if (this.activeItem?.checked) {
            delete this.activeItem
        }

        this.recountItems()
        this.update({ checkedCounter: 0 })
    }

    onRandomClick = () => {
        if (!_.isEmpty(this.items) && this.items.length > 1) {
            for (let i = this.items.length - 1; i > 0; i--) {
                let j = Math.floor(Math.random() * (i + 1))

                const saved = this.items[i].number
                this.items[i].number = this.items[j].number
                this.items[j].number = saved
            }

            this.sortUpdate(this.activeItem?.number)
        }
    }

    onRowsRendered = ({ overscanStopIndex, startIndex }) => {
        this.updateMetas({ start: startIndex, stop: overscanStopIndex })
    }

    setItemsRef = (ref) => {
        this.itemsRef = ref
    }

    async updateMeta(item, includePictures) {
        const { file } = item
        const key = getFileKey(file)

        if (_.isUndefined(this.metas[key])) {
            this.metas[key] = null
            const { meta } = await call({
                type: 'apps/audio/meta',
                args: { file },
            })

            this.metas[key] = meta
            item.updateMeta(meta)
        }

        if (
            includePictures &&
            this.metas[key].picturesCount > 0 &&
            _.isUndefined(this.metas[key].picture)
        ) {
            this.metas[key].picture = null

            const { picture } = await call({
                type: 'apps/audio/meta',
                args: { file, onlyPictures: true },
            })

            this.metas[key].picture = picture
        }
    }

    async updateMetas(interval) {
        this.renderedIndexes = interval
        const { start, stop } = interval

        for (let i = start; i <= stop; i++) {
            const item = this.items[i]
            if (!item.meta) {
                await this.updateMeta(item)

                if (this.renderedIndexes != interval || this.unmounted) {
                    break
                }
            }
        }
    }

    isActiveItem(item) {
        return this.activeItem === item
    }

    async setActiveItem({ item, ended }) {
        if (this.activeItem) {
            this.activeItem.updateActive(false)
        }

        this.activeItem = item

        if (item) {
            item.updateActive(true)

            if (ended) {
                this.itemsRef.scrollToRow(item.number)
                await connectedState()

                if (this.activeItem !== item) {
                    return
                }
            }
        }

        this.parent.activeChange(item)
    }

    go({ direction, loop, ended }) {
        let next

        if (this.activeItem && this.items.length > 1) {
            const last = _.last(this.items)
            const head = _.head(this.items)
            switch (direction) {
                case 'forward':
                    if (this.activeItem === last) {
                        if (loop) {
                            next = head
                        }
                    } else {
                        const item = _.find(
                            this.items,
                            ({ number }) => number > this.activeItem.number
                        )

                        next = item
                    }

                    break

                case 'backward':
                    if (loop && this.activeItem === head) {
                        next = last
                    } else {
                        _.forEach(this.items, (item) => {
                            if (item.number === this.activeItem.number) {
                                return false
                            }

                            next = item
                        })
                    }

                    break
            }
        }

        if (next || ended) {
            this.setActiveItem({ item: next, ended })
        }
    }

    itemHandlers = {
        doubleClick: (item) => {
            this.setActiveItem({ item })
        },

        checkedChange: (item, checked) => {
            this.update({
                checkedCounter: checked
                    ? this.checkedCounter + 1
                    : this.checkedCounter - 1,
            })
        },
    }

    onMount() {
        if (!_.isEmpty(this.items)) {
            const item = _.head(this.items)
            this.setActiveItem({ item })
        }
    }
}

export default Playlist
