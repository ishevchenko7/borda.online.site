import _ from 'lodash'

import { getName } from '#utils/file'
import { getMediaType, getFileKey } from '#utils/file'
import { call } from '#remote'
import PureModel from '#common/viewmo'
import workspace from '#models/workspace' 

import Player from './player'
import Controls from './controls'
import Playlist from './playlist'
import render from './render'

const defaultVolume = 90

class AudioApp extends PureModel {
    constructor(params) {
        super({ render })

        _.assign(this, {
            params,
            steps: {},
            player: PureModel.stub,
            controls: PureModel.stub,
            playlist: PureModel.stub,
            volume: defaultVolume,
        })
    }

    hydrateSteps(steps) {
        for (let i = 0; i < steps.length; i++) {
            const [time, position] = steps[i]
            if (i === steps.length - 1) {
                steps[i] = {
                    time,
                    position,
                }
            } else {
                const [nextTime, nextPosition] = steps[i + 1]
                steps[i] = {
                    time,
                    position,
                    duration: nextTime - time,
                    length: nextPosition - position,
                }
            }
        }

        return steps
    }

    async getSteps(file) {
        const key = getFileKey(file)

        if (this.steps[key]) {
            return this.steps[key]
        }

        const { steps } = await call({
            type: 'apps/audio/steps',
            args: { file },
        })

        this.steps[key] = this.hydrateSteps(steps)
        return steps
    }

    append = (files) => {
        this.playlist.append({ files, activate: true })
    }

    initMediaSession({ meta, fileName }) {
        if ('mediaSession' in navigator) {
            navigator.mediaSession.metadata = new MediaMetadata({
                artist: meta.artist,
                title: meta.title || getName(fileName),
            })

            if (meta.picture) {
                if (this.metadataUrl) {
                    window.URL.revokeObjectURL(this.metadataUrl)
                }

                this.metadataUrl = window.URL.createObjectURL(
                    new Blob([meta.picture.data])
                )

                navigator.mediaSession.metadata.artwork = [
                    {
                        type: 'image/jpeg',
                        sizes: meta.picture.sizes,
                        src: this.metadataUrl,
                    },
                ]
            }
        }
    }

    async playItem(item) {
        await this.playlist.updateMeta(item, true)

        const { file, meta } = item
        this.initMediaSession({ meta, fileName: file.name })
        const steps = _.cloneDeep(await this.getSteps(file))

        // Prevent play latecomer item
        if (this.playlist.isActiveItem(item)) {
            const { position: payloadLength, time: duration } = _.last(steps)

            this.controls.init({ duration, file, meta })

            const player = new Player({
                file,
                steps,
                duration,
                payloadLength,
                volume: this.volume,
                parent: this.playerHandlers,
            })

            this.update({ duration, player })
        }
    }

    onDrop = async (e) => {
        e.preventDefault()

        const dragNumber = e.dataTransfer.getData('number')        
        const files = await workspace.dropData(dragNumber)
        const audioFiles = _.filter(
            files,
            (file) => getMediaType(file.name) === 'audio'
        )
        if (!_.isEmpty(audioFiles)) {
            this.playlist.append({ files: audioFiles })
        }
    }

    onDragOver = (ev) => {
        ev.preventDefault()
    }

    //#region handlers

    playerHandlers = {
        timeUpdate: (time) => {
            this.controls.timeUpdate(time)
        },

        progress: (progress) => {
            this.controls.progressUpdate(progress)
        },

        ended: () => {
            this.playlist.go({
                ended: true,
                direction: 'forward',
                loop: this.controls.loop,
            })
        },

        pause: () => {
            this.controls.updatePaused(true)
        },

        play: () => {
            this.controls.updatePaused(false)
        },
    }

    controlsHandlers = {
        progressClick: (ratio) => {
            const time = Math.round(this.duration * ratio)
            this.player.setCurrentTime(time)
        },

        pauseClick: () => {
            this.player.playToggle()
            this.controls.updatePaused(this.player.paused)
        },

        backwardClick: () => {
            this.playlist.go({
                direction: 'backward',
                loop: this.controls.loop,
            })
        },

        forwardClick: () => {
            this.playlist.go({
                direction: 'forward',
                loop: this.controls.loop,
            })
        },

        changeVolume: (volume) => {
            this.volume = volume
            this.player.setVolume(volume)
        },
    }

    playlistHandlers = {
        activeChange: (item) => {
            if (item) {
                this.playItem(item)
            }
        },
    }

    //#endregion

    onMount() {
        const controls = new Controls({
            volume: defaultVolume,
            parent: this.controlsHandlers,
        })

        const playlist = new Playlist({
            files: this.params.files,
            parent: this.playlistHandlers,
        })

        this.update({ controls, playlist })
    }
}

export default AudioApp
