import _ from 'lodash'
import React from 'react'

import styles from './styles.scss'

export default function () {
    const { player, controls, playlist } = this
    return (
        <div
            className={styles.container}
            onDrop={this.onDrop}
            onDragOver={this.onDragOver}
        >
            <player.View />
            <controls.View />
            <playlist.View />
        </div>
    )
}
