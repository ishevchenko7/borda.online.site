import _ from 'lodash'
import PureModel from '#common/viewmo'

import render from './render'

class Controls extends PureModel {
    constructor(params) {
        super({ type: 'Audio/Player/Controls', render })
        _.assign(this, { ...params })

        this.initMediaSession()
    }

    initMediaSession() {
        if ('mediaSession' in navigator) {
            navigator.mediaSession.setActionHandler(
                'nexttrack',
                this.onForwardClick
            )
            navigator.mediaSession.setActionHandler(
                'previoustrack',
                this.onBackwardClick
            )
        }
    }

    init = (params) => {
        this.update({
            ...params,
            time: 0,
            progress: 0,
            paused: false,
        })
    }

    timeUpdate = (time) => {
        this.update({ time })
    }

    progressUpdate = (progress) => {
        this.update({ progress })
    }

    updatePaused = (paused) => {
        if (paused != this.paused) {
            this.update({ paused })
        }
    }

    onProgressClick = (e) => {
        const rect = this.progressRef.getBoundingClientRect()
        const x = e.clientX - rect.left
        const ratio = x / this.progressRef.clientWidth
        this.update({ time: this.duration * ratio })

        this.parent.progressClick(ratio)
    }

    onPauseClick = () => {
        this.parent.pauseClick()
    }

    onBackwardClick = () => {
        this.parent.backwardClick()
    }

    onForwardClick = () => {
        this.parent.forwardClick()
    }

    onLoopClick = () => {
        this.update({ loop: !this.loop })
    }

    onChangeVolume = (volume) => {
        this.parent.changeVolume(volume)
    }

    setProgressRef = (ref) => {
        this.progressRef = ref
    }
}

export default Controls
