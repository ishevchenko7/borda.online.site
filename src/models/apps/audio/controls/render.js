import _ from 'lodash'
import React from 'react'
import { Popover, Slider } from 'antd'

import { Button } from '#components'
import { getTitle } from '#utils/mp3'
import { seconds2Time } from '#utils/time'
import styles from './styles.scss'

export default function () {
    const { title = '', artist = '' } = this.meta || {}
    return (
        <div className={styles.container}>
            <div className={styles.buttons}>
                <Button.Group size='large'>
                    <Button icon='backward' onClick={this.onBackwardClick} />
                    <Button
                        icon={this.paused ? 'play' : 'pause'}
                        onClick={this.onPauseClick}
                    />
                    <Button icon='forward' onClick={this.onForwardClick} />
                </Button.Group>
                <Button.Group size='large'>
                    <Popover
                        placement='bottom'
                        content={
                            <Slider
                                className={styles.volume}
                                vertical
                                defaultValue={this.volume}
                                onChange={this.onChangeVolume}
                            />
                        }
                        trigger='click'
                    >
                        <Button icon='speaker' />
                    </Popover>
                    <Button
                        icon='loop'
                        pale={!this.loop}
                        onClick={this.onLoopClick}
                    />
                </Button.Group>
            </div>
            <div
                className={styles.progress_container}
                onClick={this.onProgressClick}
            >
                <div className={styles.title_time}>
                    <div className={styles.title}>
                        {getTitle({ title, artist, fileName: this.file?.name })}
                    </div>

                    <div className={styles.time}>
                        {this.time && seconds2Time(this.time) + '/'}
                        {seconds2Time(this.duration)}
                    </div>
                </div>
                <div className={styles.progress_full} ref={this.setProgressRef}>
                    <div
                        className={styles.time_line}
                        style={{
                            width: `${Math.ceil(
                                (this.time / this.duration) * 100
                            )}%`,
                        }}
                    />
                    <div
                        className={styles.progress}
                        style={{
                            width: `${Math.ceil(
                                (this.progress / this.duration) * 100
                            )}%`,
                        }}
                    />
                </div>
            </div>
        </div>
    )
}
