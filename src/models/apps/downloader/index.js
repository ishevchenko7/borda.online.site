import _ from 'lodash'
import React from 'react'
import { Modal } from 'antd'

import { Button } from '#components'
import { getFileKey } from '#utils/file'
import PureModel from '#common/viewmo'
import Item from './item'
import render from './render'

class Downloader extends PureModel {
    constructor() {
        super({ render })

        _.assign(this, {
            items: {},
            finished: [],
        })
    }

    onEraserClick = () => {
        if (!_.isEmpty(this.finished)) {
            _.forEach(this.finished, (key) => delete this.items[key])
            this.update({ finished: [] })
        }
    }

    downloadFiles = async (files) => {
        if (!_.isEmpty(files)) {
            let needClean = false
            _.forEach(files, (file) => {
                const key = getFileKey(file)
                if (!this.items[key]) {
                    this.items[key] = new Item({
                        key,
                        file,
                        parent: this.itemHandlers,
                    })
                } else {
                    needClean = true
                }
            })

            if (needClean) {
                Modal.warn({
                    title: 'Double download',
                    content: (
                        <div>
                            <span>
                                If you need to download file again, please open
                                downloader and clean finished files. <br />
                                Use <Button icon='eraser' /> button for it.
                            </span>
                        </div>
                    ),
                })
            }

            this.update()
        }
    }

    itemHandlers = {
        finish: ({ key }) => {
            this.finished.push(key)
        },
    }
}

export default Downloader
