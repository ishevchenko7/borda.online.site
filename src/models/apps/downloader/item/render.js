import _ from 'lodash'
import React from 'react'

import { Progress, Button } from 'antd'
import {
    CaretRightOutlined,
    PauseOutlined,
    StopOutlined,
} from '@ant-design/icons'

import styles from './styles.scss'

export default function () {
    const disabled = this.stopped || this.finished
    return (
        <div className={styles.container}>
            <div>{this.file.name}</div>
            <div>{this.totalText}</div>
            <div>
                <Progress
                    type='circle'
                    width={40}
                    percent={this.percent}
                    {...(this.stopped ? { status: 'exception' } : {})}
                />
            </div>
            <div>
                <Button.Group>
                    <Button
                        icon={
                            this.paused ? <CaretRightOutlined /> : <PauseOutlined />
                        }
                        onClick={this.onPauseClick}
                        disabled={disabled}
                    />
                    <Button
                        icon={<StopOutlined />}
                        onClick={this.onStopClick}
                        disabled={disabled}
                    />
                </Button.Group>
            </div>
        </div>
    )
}