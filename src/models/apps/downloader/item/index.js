import _ from 'lodash'
import streamSaver from 'streamsaver'
import filesize from 'filesize'

import PureModel from '#common/viewmo'
import { call } from '#remote'

import render from './render'

//TODO const!
streamSaver.mitm = 'https://borda.online/public/StreamSaver.js/mitm.html'

class Item extends PureModel {
    constructor(params) {
        super({ type: 'Downloader/Item', render })

        _.assign(this, {
            ...params,
            paused: false,
            stopped: false,
            finished: false,
            totalRead: 0,
            lastPercent: 0,
        })

        this.downloadFile(this.file)
    }

    onPauseClick = () => {
        if (this.paused) {
            this.pauseResolver()
        }

        this.update({ paused: !this.paused })
    }

    onStopClick = () => {
        if (this.paused) {
            this.pauseResolver()
        }

        this.stop()
    }

    step(bytesRead) {
        this.totalRead += bytesRead
        const percent = Math.ceil((this.totalRead / this.total) * 100)
        if (percent - this.lastPercent > 1) {
            const totalText = filesize(this.totalRead, { base: 10, round: 0 })
            this.lastPercent = percent

            this.update({
                percent,
                totalText,
            })
        }
    }

    stop() {
        this.update({ stopped: true, finished: true })
        this.parent.finish(this)
    }

    finish() {
        this.update({ finished: true })
        this.parent.finish(this)
    }

    downloadFile = async (file) => {
        let size
        try {
            ;({ size } = await call({
                type: 'apps/downloader/file-size',
                args: { file },
            }))
        } catch (error) {
            this.stop()
            return
        }

        const name = file.type === 'dir' ? file.name + '.zip' : file.name

        this.total = size
        const fileStream = streamSaver.createWriteStream(name, { size })
        const writer = fileStream.getWriter()

        let position = 0
        do {
            let buffer
            try {
                ;({ buffer } = await call({
                    type: 'apps/downloader/read-file',
                    args: { file, position },
                }))
            } catch (error) {
                this.stop()
                return
            }

            if (buffer) {
                position += buffer.length
                await writer.write(buffer)
                this.step(buffer.length)
            } else {
                break
            }

            if (this.paused) {
                await new Promise((resolve) => {
                    this.pauseResolver = resolve
                })
            }
        } while (!this.stopped)

        if (this.stopped) {
            writer.abort()
            this.finish()
        } else {
            writer.close()
            this.finish()
        }
    }
}

export default Item
