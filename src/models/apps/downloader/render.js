import _ from 'lodash'
import React from 'react'

import { Button } from '#components'
import styles from './styles.scss'

export default function () {
    return (
        <div className={styles.container}>
            <div>
                <Button.Group size='large'>
                    <Button icon='eraser' onClick={this.onEraserClick} />
                </Button.Group>
            </div>
            <div>
                {_.transform(
                    this.items,
                    (acc, item, key) => {
                        acc.push(<item.View key={key} />)
                    },
                    []
                )}
            </div>
        </div>
    )
}