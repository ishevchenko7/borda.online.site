import _ from 'lodash'
import React from 'react'

import { Image } from '#components'
import styles from './styles.scss'

export default function () {
    return (
        <div
            draggable={true}
            onClick={this.onClick}
            className={styles.cell_wrapper}
        >
            <div className={styles.cell}>
                {this.thumbnail && (
                    <Image
                        className={styles.thumbnail}
                        image={this.thumbnail}
                    />
                )}
            </div>
        </div>
    )
}
