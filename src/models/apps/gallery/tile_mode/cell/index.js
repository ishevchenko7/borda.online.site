import _ from 'lodash'
import PureModel from '#common/viewmo'

import render from './render'

class Cell extends PureModel {
    constructor(params) {
        super({ type: 'GalleryApp/TileMode/Cell', render })
        _.assign(this, params)
    }

    onClick = () => {
        this.parent.click(this)
    }

    async updateThumbnail(thumbnail) {
        this.update({ thumbnail })
    }
}

export default Cell
