import _ from 'lodash'
import React from 'react'

import { call } from '#remote'
import { swap } from '#utils/array'
import { getFileKey } from '#utils/file'
import PureModel from '#common/viewmo'
import VirtualizedGrid from '#apps/components/virtualized_grid'
import Cell from './cell'

import render from './render'

class TileMode extends PureModel {
    constructor(params) {
        super({ type: 'GalleryApp/TileMode', render })

        _.assign(this, {
            ...params,
            thumbnails: {},
            // TODO const!
            minCellWidth: 50,
        })

        this.initialize()
    }

    //TODO const!
    initialize({ columnCount = 3 } = {}) {
        const cellsCount = this.files ? this.files.length : 0

        const virtualizedGrid = new VirtualizedGrid({
            cellsCount,
            columnCount,
            aspectRatio: 1,
            parent: this.virtualizedGridHandlers,
        })

        _.assign(this, {
            columnCount,
            thumbnails: {},
            virtualizedGrid,
            cells: new Array(cellsCount),
        })
    }

    getCell(index) {
        if (!this.cells[index]) {
            const file = this.files[index]
            const key = getFileKey(file)

            this.cells[index] = new Cell({
                key,
                file,
                thumbnail: this.thumbnails[key],
                parent: this.cellHandlers,
            })
        }

        return this.cells[index]
    }

    setFiles(files) {
        this.files = files
        this.virtualizedGrid.updateCellsCount(files.length)
    }

    //#region tool buttons

    scrollToIndex(index) {
        this.virtualizedGrid.scrollToIndex(index)
    }

    shiftColumnCount(step) {
        const { updating, columnCount } = this.virtualizedGrid
        if (!updating) {
            this.initialize({ columnCount: columnCount + step })
            this.update()
        }
    }

    plusClick = () => {
        this.shiftColumnCount(-1)
    }

    minusClick = () => {
        this.shiftColumnCount(1)
    }

    onRandomClick = (e) => {
        for (let i = this.cells.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1))

            swap(this.cells, i, j)
            swap(this.files, i, j)
        }

        this.virtualizedGrid.forceUpdate()
    }

    onCleanClick = () => {
        this.parent.cleanClick()
    }

    async updateThumbnails({ startIndex, stopIndex }) {
        const cells = []

        for (let index = startIndex; index <= stopIndex; index++) {
            const cell = this.getCell(index)

            if (_.isUndefined(this.thumbnails[cell.key])) {
                cells.push(cell)
                this.thumbnails[cell.key] = null
            }
        }

        if (!_.isEmpty(cells)) {
            const uniqCells = _.uniqBy(cells, 'key')
            const { cellWidth, cellHeight } = this.virtualizedGrid
            const { thumbnails } = await call({
                type: 'sharp/resize',
                args: {
                    width: cellWidth,
                    height: cellHeight,
                    files: _.map(uniqCells, (cell) => cell.file),
                    optionPreset: 'entropy',
                },
            })

            for (let i = 0; i < thumbnails.length; i++) {
                if (thumbnails[i]) {
                    const { key } = uniqCells[i]
                    this.thumbnails[key] = thumbnails[i]
                }
            }

            _.forEach(cells, (cell) =>
                cell.updateThumbnail(this.thumbnails[cell.key])
            )
        }
    }

    //#endregion

    virtualizedGridHandlers = {
        cellRenderer: (index) => {
            const cell = this.getCell(index)
            return <cell.View />
        },

        cellsIntervalUpdate: async (interval) => {
            await this.updateThumbnails(interval)
        },

        onSetGridRef: ({ cellWidth, columnCount }) => {
            this.update({
                plusDisabled: columnCount === 1,
                minusDisabled: cellWidth < this.minCellWidth,
            })
        },
    }

    cellHandlers = {
        click: ({ file }) => {
            const index = _.findIndex(this.files, file)
            this.parent.imageClick(index)
        },
    }
}

export default TileMode
