import React from 'react'

import { Button } from '#components'
import styles from './styles.scss'

export default function () {
    const { virtualizedGrid } = this
    
    return (
        <div className={styles.container}>
            <div className={styles.buttons}>
                <Button.Group size='large'>
                    <Button
                        icon='minus'
                        pale={this.minusDisabled}
                        disabled={this.minusDisabled}
                        onClick={this.minusClick}
                    />
                    <Button
                        icon='plus'
                        pale={this.plusDisabled}
                        disabled={this.plusDisabled}
                        onClick={this.plusClick}
                    />
                    <Button icon='random' onClick={this.onRandomClick} />
                    <Button icon='eraser' onClick={this.onCleanClick} />
                </Button.Group>
            </div>
            <div>
                <virtualizedGrid.View />
            </div>
            <div className={styles.footer}></div>
        </div>
    )
}
