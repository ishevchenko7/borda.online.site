import React from 'react'
import clsx from 'clsx'

import styles from './styles.scss'

export default function () {
    const { tileMode, slideshowMode } = this
    return (
        <div
            className={styles.container}
            onDrop={this.onDrop}
            onDragOver={this.onDragOver}
        >
            {this.mode === 'slideshow' && (
                <div className={styles.mode}>
                    <slideshowMode.View />
                </div>
            )}
            <div
                className={clsx(
                    this.mode !== 'tile' && styles.hidden,
                    styles.mode
                )}
            >
                <tileMode.View />
            </div>
            <div className={styles.footer}>
                <div className={styles.file_name}>{this.currentFileName}</div>
                <div>{this.files?.length || 0}</div>
            </div>
        </div>
    )
}
