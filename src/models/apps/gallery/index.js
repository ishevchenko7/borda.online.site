import _ from 'lodash'
import { message } from 'antd'

import { getMediaType } from '#utils/file'
import PureModel from '#common/viewmo'
import TileMode from './tile_mode'
import SlideshowMode from './slideshow_mode'
import workspace from '#models/workspace' 

import render from './render'

class GalleryApp extends PureModel {
    constructor(params) {
        super({ render })

        _.assign(this, params)
        this.initialize()
    }

    initialize() {
        const tileMode = new TileMode({
            files: this.files,
            parent: this.tileModeHandlers,
        })
        const slideshowMode = new SlideshowMode({
            files: this.files,
            parent: this.slideshowModeHandlers,
        })

        const mode =
            this.files && this.files.length === 1 ? 'slideshow' : 'tile'

        _.assign(this, {
            mode,
            tileMode,
            slideshowMode,
            dropKeys: [],
        })
    }

    switchMode() {
        this.mode = this.mode === 'tile' ? 'slideshow' : 'tile'

        if (this.mode === 'tile') {
            this.currentFileName = ''
        }

        this.update()
    }

    onDrop = (e) => {
        e.preventDefault()
        const key = e.dataTransfer.getData('key')
        this.dropKeys[key] = key
    }

    onDrop = async (e) => {
        e.preventDefault()

        const dragNumber = e.dataTransfer.getData('number')        
        const files = await workspace.dropData(dragNumber)
        const imageFiles = _.filter(
            files,
            (file) => getMediaType(file.name) === 'image'
        )
        this.appendFiles(imageFiles)
    }

    onDragOver = (ev) => {
        ev.preventDefault()
    }

    appendFiles(files) {
        if (!_.isEmpty(files)) {
            this.files = _.concat(this.files || [], files)
            this.tileMode.setFiles(this.files)
            this.slideshowMode.setFiles(this.files)

            this.update()
            message.success(`${files.length} photos added to gallery playlist`)
        }
    }

    append = (files) => {
        this.appendFiles(files)

        if (files.length === 1) {
            this.slideshowMode.activeIndex = this.files.length - 1
            if (this.mode === 'tile') {
                this.switchMode()
            } else {
                this.slideshowMode.openImage()
            }
        }
    }

    tileModeHandlers = {
        imageClick: (index) => {
            this.slideshowMode.activeIndex = index
            this.switchMode()
        },

        cleanClick: () => {
            this.files = []
            this.initialize()
            this.update()
        },
    }

    slideshowModeHandlers = {
        exit: () => {
            this.switchMode()
        },

        openImage: (index) => {
            this.update({
                currentFileIndex: index,
                currentFileName: this.files[index].name,
            })
        },
    }

    onUnmount() {
        this.slideshowMode.cleanCache()
    }
}

export default GalleryApp
