import React from 'react'
import { Popover, Slider } from 'antd'

import { Button, Image } from '#components'
import styles from './styles.scss'

export default function () {
    return (
        <div className={styles.container}>
            <div className={styles.buttons}>
                <div>
                    <Button.Group size='large'>
                        <Button icon='left-arrow' onClick={this.onExitClick} />
                    </Button.Group>
                    <Button.Group size='large'>
                        <Button
                            icon='backward'
                            onClick={this.onBackwardClick}
                        />
                        <Button
                            icon={this.played ? 'pause' : 'play'}
                            onClick={this.onPlayClick}
                        />
                        <Button icon='forward' onClick={this.onForwardClick} />
                    </Button.Group>
                    <Button.Group size='large'>
                        <Popover
                            placement='bottom'
                            content={
                                <Slider
                                    className={styles.volume}
                                    min={1}
                                    max={25}
                                    defaultValue={this.delay}
                                    onChange={this.onChangeDelay}
                                />
                            }
                            trigger='click'
                        >
                            <Button icon='clock' />
                        </Popover>
                        <Button
                            icon='loop'
                            pale={!this.loop}
                            onClick={this.onLoopClick}
                        />
                    </Button.Group>

                    <Button.Group size='large' onClick={this.onFullscreenClick}>
                        <Button icon='fullscreen' />
                    </Button.Group>
                </div>
            </div>
            <div
                className={styles.image}
                ref={this.setImageRef}
                onClick={this.onImageClick}
            >
                {this.image && <Image image={this.image} />}
            </div>
        </div>
    )
}