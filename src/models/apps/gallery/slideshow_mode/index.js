import _ from 'lodash'
import screenfull from 'screenfull'

import logger from '#logger'
import { getFileKey } from '#utils/file'
import PureModel from '#common/viewmo'
import { call, connectedState } from '#remote'

import render from './render'

//TODO const!
const defaultDelay = 3
const cacheForwardDelta = 5
const cacheBackwardDelta = 2

class SlideshowMode extends PureModel {
    constructor(params) {
        super({ type: 'GalleryApp/SlideshowMode', render })

        const screenWidth = Math.trunc(
            window.screen.width * window.devicePixelRatio
        )

        _.assign(this, {
            ...params,
            images: [],
            screenWidth,
            paused: true,
            delay: defaultDelay,
        })
    }

    async pushToCache(file, key) {
        const cached = _.find(this.images, (image) => image.key == key)

        if (cached) {
            return cached.image
        } else {
            try {
                const { thumbnails } = await call({
                    type: 'sharp/resize',
                    args: {
                        width: this.screenWidth,
                        files: [file],
                        optionPreset: 'entropy',
                    },
                })

                const image = thumbnails[0]
                this.images.push({ key, image })

                return image
            } catch (error) {
                this.images.push({ key })
                logger.error(
                    `Adding image ${key} to cache raise error: ${error}`
                )
            }
        }
    }

    needStopCaching() {
        if (this.stopCachingResolver) {
            this.stopCachingResolver()
            return true
        }
    }

    async stopCaching() {
        if (this.caching) {
            await new Promise((resolve) => {
                this.stopCachingResolver = resolve
            })

            this.stopCachingResolver = undefined
        }
    }

    async moveCache(index) {
        await this.stopCaching()

        try {
            this.caching = true

            const toCache = []
            const start = Math.min(
                this.files.length - 1,
                index + cacheForwardDelta
            )
            const stop = Math.max(0, index - cacheBackwardDelta)
            for (let i = start; i >= stop; i--) {
                const file = this.files[i]
                const key = getFileKey(file)
                toCache.push({ key, file })
            }

            // Push to cache
            for (const { file, key } of toCache) {
                if (this.needStopCaching()) {
                    break
                }

                await this.pushToCache(file, key)
            }

            // Clean cache
            this.images = _.filter(this.images, (image) =>
                _.find(toCache, ({ key }) => key === image.key)
            )
        } finally {
            this.caching = false
        }
    }

    async getImage(index) {
        const file = this.files[index]

        if (file) {
            const key = getFileKey(file)
            const image = await this.pushToCache(file, key)
            this.moveCache(index)
            return image
        }
    }

    async openImage({ activeIndex, autoFlag = false } = {}) {
        if (autoFlag) {
            await connectedState()

            if (!this.mounted || !this.played) {
                return false
            }
        }

        if (!_.isUndefined(activeIndex)) {
            this.activeIndex = activeIndex
        }

        const currentActiveIndex = this.activeIndex
        const image = await this.getImage(this.activeIndex)

        if (autoFlag && !this.played) {
            return false
        }

        if (image && this.mounted && this.activeIndex === currentActiveIndex) {
            this.update({ image })
            this.parent.openImage(this.activeIndex)

            return true
        }
    }

    setFiles(files) {
        this.files = files
    }

    async goForward(autoFlag) {
        if (this.activeIndex < this.files.length - 1 || this.loop) {
            this.activeIndex++

            if (this.activeIndex === this.files.length) {
                this.activeIndex = 0
            }

            await this.openImage({ autoFlag })
            return true
        }

        return false
    }

    async goBack() {
        if (this.activeIndex > 0) {
            this.activeIndex--
            await this.openImage()

            return true
        }
    }

    onBackwardClick = () => {
        this.goBack()

        if (this.played) {
            this.play()
        }
    }

    onForwardClick = () => {
        this.goForward()

        if (this.played) {
            this.play()
        }
    }

    onImageClick = (e) => {
        if (screenfull.isFullscreen) {
            if (e.pageX < window.screen.width / 2) {
                this.onBackwardClick()
            } else {
                this.onForwardClick()
            }
        }
    }

    stop() {
        this.update({ played: false })
        clearTimeout(this.playTimer)
    }

    async play() {
        clearTimeout(this.playTimer)

        const playLoop = async () => {
            if (this.mounted && (await this.goForward(true))) {
                go()
            } else {
                this.stop()
            }
        }

        const go = () => {
            this.playTimer = setTimeout(() => playLoop(), this.delay * 1000)
        }

        go()
    }

    cleanCache() {
        this.images = undefined
    }

    onPlayClick = () => {
        if (this.played) {
            this.update({ played: false })
            clearTimeout(this.playTimer)
        } else {
            this.update({ played: true })
            this.play()
        }
    }

    onExitClick = () => {
        this.stop()
        this.update({ activeIndex: -1, image: undefined })
        this.parent.exit()
    }

    onFullscreenClick = () => {
        if (screenfull.isEnabled) {
            screenfull.request(this.imageRef)
        }
    }

    onLoopClick = () => {
        this.update({ loop: !this.loop })
    }

    onChangeDelay = (delay) => {
        this.delay = delay
    }

    setImageRef = (ref) => {
        this.imageRef = ref
    }

    onMount() {
        if (_.isUndefined(this.activeIndex)) {
            this.activeIndex = 0
        }

        this.openImage()
    }

    onUnmount() {
        this.stopCaching()
    }
}

export default SlideshowMode
