import * as settingsStore from '#common/settings-store'

const instances = {}

export async function create(name, params) {
    if (instances[name]) {
        return instances[name]
    } else {
        const { default: App } = await import(`#apps/${name}`)
        const app = new App(params)
        const { persistentInstance } = settingsStore.get().apps[name]

        if (persistentInstance) {
            instances[name] = app
        }

        return app
    }
}
