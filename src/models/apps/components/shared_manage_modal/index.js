import _ from 'lodash'
import PureModel from '#common/viewmo'
import ShareManageForm from './shared_manage_form'

import render from './render'

class SharedManageModal extends PureModel {
    constructor(params) {
        super({ render })
        _.assign(this, params)
    }

    show = (record) => {
        const shareManageForm = new ShareManageForm({ record })
        this.update({
            visible: true,
            shareManageForm,
        })
    }

    hide = () => {
        this.update({
            visible: false,
        })
    }

    onOk = async () => {
        const fields = await this.shareManageForm.getFields()

        this.hide()
        this.parent.onConfirm(fields)
    }

    onCancel = () => {
        this.hide()
    }
}

export default SharedManageModal
