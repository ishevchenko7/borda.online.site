import React from 'react'
import { Button, Form, Input, Switch, Select, Divider } from 'antd'

import styles from './styles.scss'

const { Option } = Select

const formLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
}

export default function () {
    return (
        <Form
            {...formLayout}
            name='manageShared'
            initialValues={this.initialValues}
            className={styles.form}
            ref={this.setFormRef}
        >
            <Form.Item hidden={true} name='id'>
                <Input />
            </Form.Item>
            <Form.Item hidden={true} name='name'>
                <Input />
            </Form.Item>
            <Form.Item label='Enable URL'>
                <Switch
                    onChange={this.urlEnabledChange}
                    checked={!this.urlDisabled}
                />
            </Form.Item>
            <Form.Item label='URL path' className={styles.url_path}>
                <Form.Item
                    name='url_path'
                    noStyle
                    hasFeedback
                    rules={[
                        (formRef) => ({
                            validator: (rule, value) => {
                                let error
                                if (!this.urlDisabled) {
                                    error = !value ? 'Not empty required' : ''

                                    if (!error) {
                                        if (
                                            !RegExp(
                                                `^[${this.urlPathCharacters}]+$`
                                            ).test(value)
                                        ) {
                                            error = 'Contains unallowable chars'
                                        }
                                    }
                                }

                                if (error) {
                                    this.setUrl()
                                    return Promise.reject(error)
                                } else {
                                    this.setUrl(value)
                                    return Promise.resolve()
                                }
                            },
                        }),
                    ]}
                >
                    <Input disabled={this.urlDisabled}></Input>
                </Form.Item>
                <Button
                    disabled={this.urlDisabled}
                    onClick={this.generateClick}
                >
                    Generate
                </Button>
            </Form.Item>
            <Form.Item label='URL'>
                <fieldset disabled>
                    <Form.Item name='url'>
                        <Input disabled={this.urlDisabled} />
                    </Form.Item>
                </fieldset>
            </Form.Item>
            <Divider />
            {this.queryPaths.action && (
                <Form.Item
                    name={['query', ...this.queryPaths.action]}
                    label='Action'
                >
                    <Select>
                        <Option key='play' value='play'>
                            Play
                        </Option>
                        <Option key='download' value='download'>
                            Download
                        </Option>
                    </Select>
                </Form.Item>
            )}
        </Form>
    )
}
