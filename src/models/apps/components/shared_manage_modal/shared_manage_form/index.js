import _ from 'lodash'
import qs from 'qs'
import { customAlphabet } from 'nanoid'

import PureModel from '#common/viewmo'
import * as settingsStore from '#common/settings-store'
import withSettings from '#common/settings-store/with_settings'

import render from './render'

const sharedTypeQueryPaths = {
    dir: {
        action: ['action'],
    },
    file: {
        action: ['action'],
    },
}

class SharedManageForm extends PureModel {
    constructor({ record }) {
        super({ render })

        const { query, ...fields } = record

        if (query) {
            fields.query = qs.parse(query)
        }

        const { url_path } = fields
        const { urlPathCharacters } = settingsStore.get().validation

        fields.url = this.getUrl(url_path)

        _.assign(this, {
            urlPathCharacters,
            initialValues: fields,
            urlDisabled: !url_path,
            queryPaths: sharedTypeQueryPaths[fields.type] || [],
        })

        withSettings(this, 'sharedManageForm')
    }

    setFormRef = (ref) => {
        this.formRef = ref
    }

    async getFields() {
        const {
            id,
            name,
            query,
            url_path,
        } = await this.formRef.validateFields()

        return {
            id,
            name,
            query: query ? qs.stringify(query) : null,
            url_path: this.urlDisabled ? null : url_path,
        }
    }

    getUrl = (urlPath) => {
        return urlPath ? `${window.location.origin}/${urlPath}` : ''
    }

    setUrl = (urlPath) => {
        this.formRef.setFieldsValue({
            url: this.getUrl(urlPath),
        })
    }

    urlEnabledChange = (value) => {
        const urlDisabled = !value

        if (urlDisabled) {
            this.setUrl()
            this.formRef.setFields([
                { name: 'url_path', value: null, errors: [] },
            ])
        } else {
            const url_path =
                this.formRef.getFieldValue('url_path') || null
            if (!url_path) {
                this.generateUrlPath()
            } else {
                this.setUrl(url_path)
            }
        }

        this.update({ urlDisabled })
    }

    generateUrlPath() {
        const { urlPathLength } = this.settings
        const nanoid = customAlphabet(this.urlPathCharacters, urlPathLength)

        const url_path = nanoid()
        this.setUrl(url_path)
        this.formRef.setFieldsValue({ url_path })
    }

    generateClick = () => {
        this.generateUrlPath()
    }
}

export default SharedManageForm
