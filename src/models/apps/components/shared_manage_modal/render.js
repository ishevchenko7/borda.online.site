import React from 'react'
import { Button, Modal } from 'antd'

import styles from './styles.scss'

export default function () {
    const { shareManageForm } = this

    return this.visible ? (
        <Modal
            className={styles.container}
            visible={true}
            onCancel={this.onCancel}
            maskClosable={false}
            title={'Manage'}
            footer={[
                <Button key='ok' type='primary' onClick={this.onOk}>
                    OK
                </Button>,
                <Button key='cancel' type='primary' onClick={this.onCancel}>
                    Cancel
                </Button>,
            ]}
        >
            <shareManageForm.View />
        </Modal>
    ) : null
}
