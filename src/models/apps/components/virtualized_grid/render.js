import _ from 'lodash'
import React from 'react'
import { Grid, AutoSizer } from 'react-virtualized'

import styles from './styles.scss'

export default function() {
    return (
        <div
            key={this.updateCounter}
            ref={this.setVirtualizedGridRef}
            className={styles.virtualized_grid}
        >
            <AutoSizer>
                {({ width, height }) => {
                    if (width === 0) return null
                    this.calculateSizes({ width })

                    return (
                        <Grid
                            ref={this.setGridRef}
                            className={styles.grid}
                            cellRenderer={this.cellRenderer}
                            onSectionRendered={this.onSectionRendered}
                            onScroll={this.onScroll}
                            columnCount={this.columnCount}
                            columnWidth={this.cellWidth}
                            height={height}
                            rowCount={this.rowCount}
                            rowHeight={this.cellHeight}
                            width={width}
                            scrollToAlignment='center'
                            overscanRowCount={this.settings.overscanRowCount}
                        />
                    )
                }}
            </AutoSizer>
        </div>
    )
}
