import _ from 'lodash'
import React from 'react'
import { scrollbarWidth } from '@xobotyi/scrollbar-width'

import PureModel from '#common/viewmo'
import withSettings from '#common/settings-store/with_settings'

import render from './render'

class VirtualizedGrid extends PureModel {
    constructor(params) {
        super({ type: 'Components/Grid', render })

        withSettings(this, 'virtualizedGrid')

        _.assign(this, {
            ...params,
            updateCounter: 0,
            onIntervalsRendered: _.debounce(
                this.onIntervalsRendered,
                this.settings.scrollDebounceTime
            ),
        })
    }

    cellRenderer = ({ columnIndex, key, rowIndex, style }) => {
        const index = this.columnCount * rowIndex + columnIndex
        const cell = index < this.cellsCount && this.parent.cellRenderer(index)

        return cell ? (
            <div key={key} style={style}>
                {cell}
            </div>
        ) : null
    }

    splitIntervals({ startIndex, stopIndex, reverse }) {
        const result = []
        const length = stopIndex - startIndex + 1
        const chunkLength = this.settings.imagesPerOneRequest

        startIndex = Math.max(startIndex, 0)
        stopIndex = Math.min(stopIndex, this.cellsCount - 1)

        if (length > chunkLength) {
            const step = chunkLength - 1
            for (let i = 0; i < length; i += chunkLength) {
                const a = i
                let b

                if (i === 0 && length % chunkLength > 0) {
                    b = (length % chunkLength) - 1
                    i += b - step
                } else {
                    b = i + step
                }

                result.push({
                    startIndex: a + startIndex,
                    stopIndex: b + startIndex,
                })
            }
        } else {
            return [{ startIndex, stopIndex }]
        }

        return reverse ? _.reverse(result) : result
    }

    //TODO const!
    async onIntervalsRendered(intervals) {
        if (!this.unmounted) {
            try {
                this.updating = true
                await Promise.all(
                    _.map(intervals, (interval) =>
                        this.parent.cellsIntervalUpdate(interval)
                    )
                )
            } finally {
                this.updating = false
            }
        }
    }

    onSectionRendered = ({
        columnOverscanStartIndex,
        columnOverscanStopIndex,
        columnStartIndex,
        columnStopIndex,
        rowOverscanStartIndex,
        rowOverscanStopIndex,
        rowStartIndex,
        rowStopIndex,
    }) => {
        const startIndex = rowStartIndex * this.columnCount + columnStartIndex
        const stopIndex = Math.min(
            rowStopIndex * this.columnCount + columnStopIndex,
            this.cellsCount - 1
        )
        const startOverscanIndex =
            rowOverscanStartIndex * this.columnCount + columnOverscanStartIndex
        const stopOverscanIndex = Math.min(
            rowOverscanStopIndex * this.columnCount + columnOverscanStopIndex,
            this.cellsCount - 1
        )

        const overscanIntervals = [
            { startIndex: stopIndex + 1, stopIndex: stopOverscanIndex },
            {
                startIndex: startOverscanIndex,
                stopIndex: startIndex - 1,
                reverse: true,
            },
        ]

        const intervals = [
            { startIndex, stopIndex },
            ..._.sortBy(
                overscanIntervals,
                ({ startIndex, stopIndex }) => startIndex - stopIndex
            ),
        ]

        const chunked = _.reduce(
            intervals,
            (result, value) => {
                const { startIndex, stopIndex } = value
                if (startIndex <= stopIndex) {
                    result.push(...this.splitIntervals(value))
                }
                return result
            },
            []
        )

        this.onIntervalsRendered(chunked)
    }

    scrollToIndex(index) {
        const rowIndex = Math.floor(index / this.columnCount)
        this.gridRef.scrollToCell({ rowIndex })
    }

    updateCellsCount(cellsCount) {
        this.update({ cellsCount, updateCounter: ++this.updateCounter })
    }

    forceUpdate() {
        this.update({ updateCounter: ++this.updateCounter })
    }

    onScroll = ({ scrollTop }) => {
        this.scrollTopPosition = scrollTop
    }

    get width() {
        return this.virtualizedGridRef.getBoundingClientRect().width
    }

    setVirtualizedGridRef = (ref) => {
        this.virtualizedGridRef = ref
    }

    setGridRef = (ref) => {
        this.gridRef = ref

        if (ref) {
            if (
                this.defaultScrollTopPosition &&
                this.defaultScrollTopPosition.width === this.width
            ) {
                ref.scrollToPosition({
                    scrollTop: this.defaultScrollTopPosition.scrollTopPosition,
                })
            }

            this.parent.onSetGridRef(this)
        }
    }

    calculateSizes({ width }) {
        const actualWidth = Math.max(0, width - scrollbarWidth())
        if (this.cellWidth) {
            this.columnCount = Math.floor(actualWidth / this.cellWidth) || 1
        } else {
            this.cellWidth = Math.floor(actualWidth / this.columnCount)
            this.cellHeight = Math.floor(this.cellWidth * this.aspectRatio)
        }

        this.rowCount = Math.ceil(this.cellsCount / this.columnCount)
    }
}

export default VirtualizedGrid
