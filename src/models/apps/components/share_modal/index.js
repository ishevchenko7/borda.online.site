import _ from 'lodash'
import React from 'react'
import { Form, Input, Checkbox, Table, Tag, Button } from 'antd'

import { call } from '#remote'
import PureModal from '#common/viewmo'
import SharedManageModal from '#apps/components/shared_manage_modal'

import render from './render'
import styles from './styles.scss'

class ShareModal extends PureModal {
    constructor(params) {
        super({ render })

        _.assign(this, params)

        this.columns = [
            {
                title: 'Alias',
                dataIndex: 'alias',
                render: () => (
                    <Form.Item name='alias'>
                        <Input />
                    </Form.Item>
                ),
            },
            {
                title: 'Anonymous (for all)',
                dataIndex: 'anonymous',
                width: 120,
                render: () => (
                    <Form.Item name='anonymous' valuePropName='checked'>
                        <Checkbox />
                    </Form.Item>
                ),
            },
            {
                title: 'URL Enabled',
                dataIndex: 'url_path',
                width: 100,
                render: (value) => (
                    <div className={styles.url_enabled_cell}>
                        <Tag color={Boolean(value) && 'green'}>
                            {value ? 'Yes' : 'No'}
                        </Tag>
                    </div>
                ),
            },
            {
                width: 100,
                render: (value, record) => (
                    <Button 
                        type='link'
                        onClick={() => this.shareManage(record)}
                    >
                        Manage
                    </Button>
                ),
            },
        ]
    }

    setFormRefs(index) {
        return (ref) => {
            this.formRefs[index] = ref
        }
    }

    shareManage(record) {
        this.sharedManageModal.show(record)
    }

    show = ({ items }) => {
        this.data = items

        const components = {
            body: {
                row: ({ index, record, ...props }) => (
                    <Form
                        name={'shared' + index}
                        component={false}
                        ref={this.setFormRefs(index)}
                        initialValues={record}
                    >
                        <tr {...props} />
                    </Form>
                ),
            },
        }

        const ShareTable = () => (
            <Table
                rowKey='name'
                className={styles.share_table}
                columns={this.columns}
                pagination={false}
                scroll={{ y: 800 }}
                components={components}
                onRow={this.onRow}
                dataSource={this.data}
            />
        )

        const sharedManageModal = new SharedManageModal({
            parent: this.sharedManageModalHandlers,
        })

        this.update({
            formRefs: [],
            pending: false,
            visible: true,
            ShareTable,
            sharedManageModal,
        })
    }

    onRow = (record, index) => ({
        index,
        record,
    })

    hide = () => {
        this.update({
            visible: false,
        })
    }

    async collectItems() {
        const items = []
        for (let i = 0; i < this.data.length; i++) {
            const { anonymous, alias } = await this.formRefs[i].validateFields()
            items.push({
                alias,
                anonymous,
                ..._.pick(this.data[i], [
                    'type',
                    'path',
                    'name',
                    'query',
                    'url_path',
                ]),
            })
        }

        return items
    }

    onOk = async () => {
        const items = await this.collectItems()

        try {
            if (!_.isEmpty(items)) {
                this.update({ pending: true })

                await call({
                    type: 'apps/admin-panel/shared/add',
                    args: {
                        items,
                    },
                })
            }
        } finally {
            this.hide()
        }
    }

    onCancel = () => {
        this.hide()
    }

    sharedManageModalHandlers = {
        onConfirm: (fields) => {
            const target = _.find(
                this.data,
                (record) => record.name === fields.name
            )
            _.assign(target, fields)
            this.update()
        },
    }
}

export default ShareModal
