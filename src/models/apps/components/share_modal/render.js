import React from 'react'
import { Button, Modal } from 'antd'

export default function () {
    const { ShareTable, sharedManageModal } = this

    return this.visible ? (
        <Modal
            width={800}
            visible={true}
            onCancel={this.onCancel}
            title={'Share'}
            footer={[
                <Button
                    key='ok'
                    type='primary'
                    onClick={this.onOk}
                    loading={this.pending}
                >
                    OK
                </Button>,
                <Button key='cancel' type='primary' onClick={this.onCancel}>
                    Cancel
                </Button>,
            ]}
        >
            <ShareTable />
            <sharedManageModal.View />
        </Modal>
    ) : null
}
