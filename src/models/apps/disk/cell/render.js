import _ from 'lodash'
import React from 'react'
import clsx from 'clsx'

import { getName } from '#utils/file'
import { Image } from '#components'

import { ItemSvg } from './item_svg'
import styles from './styles.scss'

export default function () {
    return (
        <div
            draggable={true}
            onClick={this.onClick}
            onDragEnd={this.onDragEnd}
            onDragStart={this.onDragStart}
            className={styles.cell_wrapper}
        >
            <div
                className={clsx(styles.cell, this.selected && styles.selected)}
            >
                {this.thumbnail ? (
                    <Image
                        style={{ height: this.thumbnailHeight }}
                        className={styles.thumbnail}
                        image={this.thumbnail}
                    />
                ) : (
                    <ItemSvg item={this.item} />
                )}
                <span className={styles.name}>
                    {(this.thumbnail || this.item.type === 'dir'
                        ? this.item.name
                        : getName(this.item.name)) +
                        (this.item.volume ? '\n' + this.item.volume : '')}
                </span>
            </div>
        </div>
    )
}
