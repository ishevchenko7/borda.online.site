import _ from 'lodash'

import PureModel from '#common/viewmo'
import render from './render' 

class Cell extends PureModel {
    constructor(params) {
        super({ type: 'Apps/Disk/Cell', render })
        _.assign(this, params)
    }

    onClick = () => {
        this.parent.click(this)
    }

    onDragEnd = (e) => {
        if (e.dataTransfer.dropEffect !== 'none') {
            this.parent.dragEnd(e, this)
        }
    }

    onDragStart = (e) => {
        this.parent.dragStart(e, this)
    }

    updateThumbnail = (thumbnail) => {
        this.update({ thumbnail })
    }

    updateSelected = (selected) => {
        this.update({ selected })
    }
}

export default Cell
