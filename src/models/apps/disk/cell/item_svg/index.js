import React from 'react'

import { getExtension } from '#utils/file'
import { SvgIcon } from '#components'
import styles from './styles.css'

const ItemSvg = ({ item }) => {
    const { type, icon, name } = item

    return type === 'dir' ? (
        <SvgIcon name={icon} height='34%' width='55%' />
    ) : (
        <svg
            xmlns='http://www.w3.org/2000/svg'
            fill='gray'
            viewBox='0 0 24 24'
            className={styles.svg}
            height='34%'
            width='55%'
        >
            <path d='M14.568.075c2.202 1.174 5.938 4.883 7.432 6.881-1.286-.9-4.044-1.657-6.091-1.179.222-1.468-.185-4.534-1.341-5.702zm-.824 7.925s1.522-8-3.335-8h-8.409v24h20v-13c0-3.419-5.247-3.745-8.256-3z' />
            <text
                x='50%'
                y='70%'
                dominantBaseline='middle'
                textAnchor='middle'
                fill='white'
                fontWeight='bold'
                fontSize='45%'
            >
                {getExtension(name).toUpperCase().substr(0, 4)}
            </text>
        </svg>
    )
}

export { ItemSvg }
