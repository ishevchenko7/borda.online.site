import React from 'react'
import { Button } from '#components'

import 'react-virtualized/styles.css'
import styles from './styles.scss'

export default function () {
    const { shareModal, virtualizedGrid } = this

    return (
        <div className={styles.container}>
            <div>
                <Button.Group size='large'>
                    <Button
                        icon='left-arrow'
                        onClick={this.levelUpClick}
                        pale={this.rootFlag}
                        disabled={this.rootFlag}
                    />
                    <Button
                        icon='minus'
                        pale={this.minusDisabled}
                        disabled={this.minusDisabled}
                        onClick={this.minusClick}
                    />
                    <Button
                        icon='plus'
                        pale={this.plusDisabled}
                        disabled={this.plusDisabled}
                        onClick={this.plusClick}
                    />
                    <Button icon='download' onClick={this.downloadClick} />
                    <Button icon='play' onClick={this.playClick} />
                    <Button icon='share' onClick={this.shareClick} />
                    <Button
                        icon='grid'
                        pale={!this.selectMode}
                        onClick={this.selectModeClick}
                    />
                </Button.Group>
            </div>
            <shareModal.View />
            <virtualizedGrid.View />
        </div>
    )
}
