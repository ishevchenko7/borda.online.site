import _ from 'lodash'
import React from 'react'
import { Modal } from 'antd'

import PureModel from '#common/viewmo'

import { call } from '#remote'
import { Button } from '#components'
import ShareModal from '#apps/components/share_modal'
import VirtualizedGrid from '#apps/components/virtualized_grid'
import { getMediaType } from '#utils/file'
import Cell from './cell'
import workspace from '#models/workspace'

import render from './render'

class DiskApp extends PureModel {
    constructor(params) {
        super({ render })

        _.assign(
            this,
            {
                cellWidth: 0,
                minCellWidth: 0,
                dragNumber: 0,
                rootFlag: true,
                selected: [],
                thumbnails: {},
                nameHeight: 40,
                scrollTopPositions: [],
                virtualizedGrid: PureModel.stub
            },
            params
        )

        this.shareModal = new ShareModal()
    }

    getCell(index) {
        if (!this.cells[index]) {
            const item = this.getItem(index)
            this.cells[index] = new Cell({
                item,
                thumbnail: this.thumbnails[item.name],
                thumbnailHeight: this.getThumbnailHeight(),
                parent: this.cellHandlers,
            })
        }

        return this.cells[index]
    }

    getItem(index) {
        const dirsCount = this.items.dirs.length

        if (index < dirsCount) {
            return {
                type: 'dir',
                dir: this.dir,
                icon: 'folder',
                name: this.items.dirs[index],
                volume: this.items.volumes && this.items.volumes[index],
            }
        } else {
            const name = this.items.files[index - dirsCount]
            const mediaType = getMediaType(name) || ''

            return {
                name,
                mediaType,
                type: 'file',
                icon: 'file' + (mediaType ? '-' + mediaType : ''),
            }
        }
    }

    getThumbnailHeight() {
        return this.virtualizedGrid.cellHeight - this.nameHeight
    }

    async updateThumbnails({ startIndex, stopIndex }) {
        const cells = []

        for (let index = startIndex; index <= stopIndex; index++) {
            const cell = this.getCell(index)
            if (cell.item.mediaType === 'image') {
                const { name } = cell.item

                if (_.isUndefined(this.thumbnails[name])) {
                    cells.push(cell)
                    this.thumbnails[name] = null
                }
            }
        }

        if (!_.isEmpty(cells)) {
            const files = _.uniq(_.map(cells, ({ item }) => item.name))

            const { thumbnails } = await call({
                type: 'sharp/resize',
                args: {
                    files,
                    dir: this.dir,
                    height: this.getThumbnailHeight(),
                    optionPreset: 'outside',
                },
            })

            for (let i = 0; i < thumbnails.length; i++) {
                this.thumbnails[files[i]] = thumbnails[i]
            }

            _.forEach(cells, (cell) => {
                const { name } = cell.item
                cell.updateThumbnail(this.thumbnails[name])
            })
        }
    }

    isPortrait() {
        return window.innerHeight > window.innerWidth
    }

    async showDir({
        dirs,
        scrollTopPosition,
        columnCount = this.isPortrait() ? 8 : 4,
    }) {
        const { dir, items, rootFlag } = await call({
            type: 'apps/disk/read-dir',
            args: { dirs },
        })

        const cellsCount = items.dirs.length + items.files.length
        const virtualizedGrid = new VirtualizedGrid({
            cellsCount,
            columnCount,
            aspectRatio: 1.1,
            defaultScrollTopPosition: scrollTopPosition,
            parent: this.virtualizedGridHandlers,
        })

        this.update({
            dir,
            items,
            rootFlag,
            cells: [],
            columnCount,
            selected: [],
            thumbnails: {},
            selectMode: false,
            virtualizedGrid,
        })
    }

    async walk(dirs) {
        const items = await call({
            type: 'apps/disk/walk',
            args: { dirs },
        })

        return _.map(items.files, ([dir, name]) => ({ dir, name }))
    }

    resetSelected(model) {
        if (!_.isEmpty(this.selected)) {
            _.forEach(this.selected, (model) => {
                model.updateSelected(false)
            })

            this.selected = []
        }

        if (model) {
            this.selected = [model]
            model.updateSelected(true)
        }
    }

    extractSelected() {
        const dirs = []
        const files = []

        _.forEach(this.selected, ({ item: { name, type, mediaType } }) => {
            if (type === 'dir') {
                dirs.push([this.dir, name])
            } else {
                files.push({ name, mediaType, dir: this.dir })
            }
        })

        return { dirs, files }
    }

    async walkSelected() {
        if (!_.isEmpty(this.selected)) {
            const { dirs, files } = this.extractSelected()
            const nestedFiles = _.isEmpty(dirs) ? [] : await this.walk(dirs)

            return _.concat(files, nestedFiles)
        }
    }

    checkSelectedNoEmpty(items) {
        if (_.isEmpty(items)) {
            Modal.warn({
                title: 'No selected items',
                content: (
                    <div>
                        <span>Please select some items.</span>
                        <br />
                        <span>
                            Use <Button icon='grid' /> button for toggle
                            selection mode.
                        </span>
                    </div>
                ),
            })

            return false
        } else {
            return true
        }
    }

    async openSelected() {
        const files = await this.walkSelected()

        if (this.checkSelectedNoEmpty(files)) {
            workspace.openFiles(files)
        }
    }

    //#region tool buttons

    shiftColumnCount(step) {
        if (!this.virtualizedGrid.updating) {
            this.thumbnails = {}
            this.decreased -= step
            this.showDir({
                dirs: [this.dir],
                columnCount: this.virtualizedGrid.columnCount + step,
            })
        }
    }

    plusClick = () => {
        this.shiftColumnCount(-1)
    }

    minusClick = () => {
        this.shiftColumnCount(1)
    }

    levelUpClick = () => {
        this.showDir({
            dirs: [this.dir, '..'],
            scrollTopPosition: this.scrollTopPositions.pop(),
            columnCount: this.virtualizedGrid.columnCount
        })
    }

    downloadClick = async () => {
        const files = _.reduce(
            this.selected,
            (result, value) => {
                const { type, name } = value.item
                result.push({
                    type,
                    name,
                    dir: this.dir,
                })

                return result
            },
            []
        )

        if (this.checkSelectedNoEmpty(files)) {
            workspace.downloadFiles(files)
        }
    }

    playClick = async () => {
        await this.openSelected()
        this.resetSelected()
    }

    selectModeClick = (e) => {
        this.update({ selectMode: !this.selectMode })
    }

    shareClick = () => {
        const selected = this.extractSelected()

        const items = []
        const add = ({ path, name, type }) =>
            items.unshift({
                type,
                path,
                name,
                alias: name,
                anonymous: true,
                url_path: null,
                query: 'action=download',
            })

        _.forEach(selected.dirs, ([path, name]) =>
            add({ type: 'dir', path, name })
        )

        _.forEach(selected.files, ({ name, dir }) =>
            add({ type: 'file', path: dir, name })
        )

        if (this.checkSelectedNoEmpty(items)) {
            this.shareModal.show({ items })
        }
    }

    //#endregion

    virtualizedGridHandlers = {
        cellRenderer: (index) => {
            const cell = this.getCell(index)
            return <cell.View />
        },

        cellsIntervalUpdate: async (interval) => {
            await this.updateThumbnails(interval)
        },

        onSetGridRef: ({ cellWidth, columnCount }) => {
            if (!this.minCellWidth) {
                this.minCellWidth = cellWidth
            }

            this.update({
                plusDisabled: columnCount === 1,
                minusDisabled: cellWidth <= this.minCellWidth,
            })
        },
    }

    cellHandlers = {
        click: (model) => {
            const { item } = model

            if (this.selectMode) {
                if (_.isEmpty(_.remove(this.selected, model))) {
                    model.updateSelected(true)
                    this.selected.push(model)
                } else {
                    model.updateSelected(false)
                }
            } else {
                this.resetSelected(model)

                if (item.type === 'dir') {
                    this.scrollTopPositions.push(
                        _.pick(this.virtualizedGrid, [
                            'width',
                            'scrollTopPosition',
                        ])
                    )

                    this.showDir({
                        dirs: _.filter([this.dir, item.name], Boolean),
                        columnCount: this.virtualizedGrid.columnCount
                    })
                } else {
                    this.openSelected()
                }
            }
        },

        dragStart: (e, model) => {
            if (!_.includes(this.selected, model)) {
                this.resetSelected(model)
            }

            this.dragNumber = workspace.dragStartData()
            e.dataTransfer.setData('number', this.dragNumber)
        },

        dragEnd: async (e, model) => {
            const files = await this.walkSelected()
            workspace.dragEndData(this.dragNumber, files)
            this.resetSelected()
        },
    }

    async onMount() {
        this.showDir({})
    }
}

export default DiskApp
