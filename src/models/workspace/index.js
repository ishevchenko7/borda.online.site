import _ from 'lodash'
import qs from 'qs'
import delay from 'delay'
import { Modal } from 'antd'
import flow from 'lodash/fp/flow'
import orderBy from 'lodash/fp/orderBy'
import groupBy from 'lodash/fp/groupBy'
import toPairs from 'lodash/fp/toPairs'
import pickBy from 'lodash/fp/pickBy'

import { call, connectedState } from '#remote'
import PureModel from '#common/viewmo'
import * as settingsStore from '#common/settings-store'

import { getUrlPath } from '#utils/url'
import { getMediaType } from '#utils/file'
import * as apps from '#apps'

import render from './render'

const appNameByMediaType = {
    audio: 'audio',
    image: 'gallery',
}

// TODO const!
const defaultFormSize = { w: 6, h: 14 }

class Workspace extends PureModel {
    constructor() {
        super({ render })

        _.assign(this, {
            apps: [],
            colsCount: 18,
            dragCounter: 0,
            dragPromises: {},
            dragResolvers: {},
        })
    }

    getApp(name) {
        return _.find(this.apps, { name })?.app
    }

    async openApp(name, params = {}) {
        let allowOpen = true
        const { singleInstance, persistentInstance } =
            settingsStore.get().apps[name]

        if (singleInstance || persistentInstance) {
            allowOpen = !Boolean(this.getApp(name))
        }

        if (allowOpen) {
            const app = await apps.create(name, params)
            this.apps.push({ name, app })
            this.update()
        }
    }

    async downloadFiles(files) {
        const app = await apps.create('downloader')
        app.downloadFiles(files)
    }

    openFiles(files) {
        const sorted = flow(
            groupBy((file) => file.mediaType || getMediaType(file.name) || ''),
            pickBy((files, mediaType) => Boolean(mediaType)),
            toPairs,
            orderBy(([type]) => type, ['asc'])
        )(files)

        _.forEach(sorted, ([mediaType, files]) => {
            const name = appNameByMediaType[mediaType]
            const app = this.getApp(name)

            if (app) {
                app.append(files)                
            } else {
                const params = { files }
                this.openApp(name, params)
            }
        })
    }

    dragStartData = () => {
        const number = this.dragCounter++
        this.dragPromises[number] = new Promise((resolve) => {
            this.dragResolvers[number] = resolve
        })

        return number
    }

    dragEndData = async (number, data) => {
        const resolver = this.dragResolvers[number]
        resolver(data)
    }

    dropData = async (number) => {
        const data = await this.dragPromises[number]

        delete this.dragPromises[number]
        delete this.dragResolvers[number]

        return data
    }

    closeApp = (app) => {
        _.remove(this.apps, { app })
        this.update()
    }

    setFormRef = async (ref) => {
        this.ref = ref

        if (this.ref) {
            await delay(300)
            this.ref.scrollIntoView({
                behavior: 'smooth',
                block: 'start',
                inline: 'nearest',
            })
        }
    }

    rectanglesOverlap(a, b) {
        return !(
            b.x >= a.x + a.w ||
            b.y >= a.y + a.h ||
            b.x + b.w <= a.x ||
            b.y + b.h <= a.y
        )
    }

    testLayoutItem(a) {
        if (a.x + a.w > this.colsCount) {
            return false
        }

        for (const b of this.layout) {
            if (this.rectanglesOverlap(a, b)) {
                return false
            }
        }

        return true
    }

    getLayoutItem = () => {
        if (!_.isEmpty(this.layout)) {
            const xs = [0]
            const ys = [0]

            _.forEach(this.layout, ({ x, y, w, h }) => {
                xs.push(x, x + w)
                ys.push(y, y + h)
            })

            let points = []
            _.forEach(_.uniq(xs), (x) => {
                _.forEach(_.uniq(ys), (y) => {
                    const point = _.find(
                        this.layout,
                        (item) => item.x === x && item.y === y
                    )
                    if (!point) {
                        points.push({ x, y })
                    }
                })
            })

            points = _.sortBy(points, ['y', 'x'])

            for (const { x, y } of points) {
                const item = { x, y, ...defaultFormSize }

                if (this.testLayoutItem(item)) {
                    return item
                }
            }
        }

        return {
            x: 0,
            y: 0,
            ...defaultFormSize,
        }
    }

    onBreakpointChange = (breakpoint, colsCount) => {
        this.colsCount = colsCount
    }

    onLayoutChange = (layout) => {
        this.layout = layout
    }

    async walk(dirs) {
        const items = await call({
            type: 'apps/disk/walk',
            args: { dirs },
        })

        return _.map(items.files, ([dir, name]) => ({ dir, name }))
    }

    async onMount() {
        const url_path = getUrlPath()

        if (url_path) {
            await connectedState()
            const { shared } = await call({
                type: 'workspace/get-shared',
                args: { url_path },
            })

            if (shared) {
                const { type, name, description } = shared
                const query = qs.parse(shared.query)
                switch (type) {
                    case 'dir':
                    case 'file':
                        if (query.action === 'play') {
                            const files =
                                type === 'file'
                                    ? [{ name }]
                                    : await this.walk([[name]])
                            this.openFiles(files)
                        } else {
                            const files = [{ dir: '', name, type }]
                            await this.downloadFiles(files)
                            this.openApp('downloader')
                        }

                        break

                    case 'webcam':
                        this.openApp('cctv', {
                            webcams: [
                                {
                                    deviceId: name,
                                    label: name,
                                    description,
                                },
                            ],
                        })
                        break
                }
            } else {
                Modal.warning({
                    title: 'Bad URL',
                    content: 'Invalid shared content name',
                })
            }
        }
    }
}

export default new Workspace()
