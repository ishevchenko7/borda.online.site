import _ from 'lodash'
import React from 'react'
import clsx from 'clsx'
import { Card } from 'antd'

import { Button } from '#components'
import styles from './styles.scss'

export default React.forwardRef(function ({ close, children }, ref) {
    return (
        <div className={styles.form_container} ref={ref}>
            <Card
                className={styles.card}
                bodyStyle={{ height: '100%', padding: '12px' }}
            >
                <div className={styles.title}>
                    <div
                        className={clsx(
                            styles.title_space,
                            'app_draggable_handle'
                        )}
                    />
                    <Button.Group size='large'>
                        <Button icon='close' onClick={close} />
                    </Button.Group>
                </div>
                <div className={styles.app_content}>{children}</div>
            </Card>
        </div>
    )
})
