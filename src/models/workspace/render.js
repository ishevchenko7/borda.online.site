import _ from 'lodash'
import React from 'react'
import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'
import { Responsive, WidthProvider } from 'react-grid-layout'

import Form from './form'

const ResponsiveReactGridLayout = WidthProvider(Responsive)

export default function () {
    return (
        <ResponsiveReactGridLayout
            breakpoints={{ lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0 }}
            cols={{ lg: 18, md: 12, sm: 6, xs: 6, xxs: 3 }}
            draggableHandle={'.app_draggable_handle'}
            rowHeight={30}
            onBreakpointChange={this.onBreakpointChange}
            onLayoutChange={this.onLayoutChange}
        >
            {_.map(this.apps, ({ app }) => (
                <div key={app.code} data-grid={this.getLayoutItem()}>
                    <Form
                        ref={this.setFormRef}
                        close={() => this.closeApp(app)}
                    >
                        <app.View />
                    </Form>
                </div>
            ))}
        </ResponsiveReactGridLayout>
    )
}
