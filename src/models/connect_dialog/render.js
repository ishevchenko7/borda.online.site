import React from 'react'
import { Modal, Button } from 'antd'

import styles from './styles.scss'

export default function () {
    return (
        <Modal
            title={null}
            closable={false}
            visible={this.visible}
            className={styles.modal}
            footer={[
                <Button key='ok' type='primary' onClick={this.onConnectClick}>
                    Connect
                </Button>,
            ]}
        >
            <p>{this.modalText}</p>
        </Modal>
    )
}
