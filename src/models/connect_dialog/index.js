import _ from 'lodash'
import React from 'react'
import delay from 'delay'
import { message } from 'antd'

import PureModel from '#common/viewmo'
import withSettings from '#common/settings-store/with_settings'

import { connect } from '#remote'
import mainMenu from '#models/main_menu'
import render from './render'

class ConnectDialog extends PureModel {
    constructor() {
        super({ render })

        _.assign(this, {
            visible: false,
        })

        withSettings(this, 'connectDialog')
    }

    onConnectClick = () => {
        this.connect()
    }

    hide() {
        this.update({
            visible: false,
        })
    }

    show(modalText) {
        this.update({
            modalText,
            visible: true,
        })
    }

    onCloseConnection = () => {
        this.connect()
    }

    async connect() {
        this.hide()

        let attemptsCounter = 0
        do {
            let currentError
            attemptsCounter++

            message.loading({
                duration: 0,
                key: 'connect',
                content: `Connection attempt №${attemptsCounter}`,
            })

            try {
                const { authenticated } = await connect(
                    this.onCloseConnection,
                    this.settings.connectionTimeout
                )

                mainMenu.connectHandler(authenticated)
                               
            } catch (error) {
                currentError = error
            } finally {
                if (currentError) {
                    if (attemptsCounter >= this.settings.maxAttemptsCount) {
                        message.destroy('connect')
                        this.show(<span>{currentError}</span>)
                        break
                    }
                } else {
                    message.success({
                        content: 'Connected',
                        key: 'connect',
                        duration: 1,
                    })
                    break
                }
            }
            await delay(this.settings.attemptDelay)
        } while (true)
    }

    onMount() {
        this.connect()
    }
}

export default new ConnectDialog()
