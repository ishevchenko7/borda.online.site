import PureModel from '#common/viewmo'

import render from './render'
import appsMenu from './apps_menu'
import loginButton from './login_button'

class MainMenu extends PureModel {
    constructor() {
        super({ render })
    }

    connectHandler(authenticated) {
        appsMenu.showMenu()
        loginButton.showConnected(authenticated)
    }
}

export default new MainMenu()