import _ from 'lodash'

import React from 'react'
import { Button } from 'antd'

import * as settingsStore from '#common/settings-store'
import { SvgIcon } from '#components'
import styles from './styles.css'

const AppButton = (name, onClick) => {
    const { title, icon } = settingsStore.get().apps[name]

    return (
        <Button key={name} name={name} onClick={onClick}>
            <SvgIcon name={icon} className={styles.svg_icon} />
            {title}
        </Button>
    )
}

export default function () {
    return (
        <Button.Group size='large'>
            {_.map(this.apps, (name) => AppButton(name, this.onClick))}
        </Button.Group>
    )
}
