import _ from 'lodash'
import { Modal } from 'antd'

import PureModel from '#common/viewmo'
import { call } from '#remote'
import workspace from '#models/workspace'
import loginButton from '#models/main_menu/login_button'

import render from './render'

class AppsMenu extends PureModel {
    constructor() {
        super({ render })
        _.assign(this, { apps: [] })
    }

    onClick = async (event) => {
        const name = event.currentTarget.getAttribute('name')
        const { hasNoContent, message } = await call({
            type: 'apps-menu/check',
            args: { name },
        })

        if (hasNoContent) {
            loginButton.show()

            Modal.warning({
                title: 'Application has no shared content to show',
                content: message,
            })
        } else {
            workspace.openApp(name)
        }
    }

    showMenu = async () => {
        const { apps } = await call({ type: 'apps-menu/available' })
        this.update({
            apps,
        })
    }
}

export default new AppsMenu()
