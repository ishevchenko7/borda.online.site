import React from 'react'

import appsMenu from './apps_menu'
import loginButton from './login_button'

import styles from './styles.css'

export default function () {
    return (
        <div className={styles.main_menu}>
            <appsMenu.View />
            <loginButton.View />
        </div>
    )
}
