import _ from 'lodash'

import { call } from '#remote'
import PureModel from '#common/viewmo'

import render from './render'
import appsMenu from '../apps_menu'

class LoginButton extends PureModel {
    constructor() {
        super({ render })
        this.inputs = {}
    }

    resetError = () => {
        if (this.error) {
            this.update({ error: false })
        }
    }

    login = async (values) => {
        this.update({ pendingStatus: 'login' })

        const { authenticated, full_name } = await call({
            type: 'authentication/login',
            args: values,
        })
        const error = !authenticated

        const usernameError =
            this.formRef.getFieldError('login')[0] ||
            (error && 'Wrong login') ||
            ''
        const passwordError =
            this.formRef.getFieldError('password')[0] ||
            (error && 'Wrong password') ||
            ''

        this.update({
            authenticated,
            full_name,
            usernameError,
            passwordError,
            pendingStatus: undefined,
        })

        if (authenticated) {
            this.hide()
            appsMenu.showMenu()
        }
    }

    logout = async () => {
        this.update({ pendingStatus: 'logout' })

        await call({ type: 'authentication/logout' })

        this.update({
            authenticated: false,
            pendingStatus: undefined,
        })

        appsMenu.showMenu()
    }

    setFormRef = (ref) => {
        this.formRef = ref
    }

    onFinish = async (values) => {
        await this.login(values)
    }

    show = () => {
        if (!this.authenticated) {
            this.update({
                visible: true,
            })
        }
    }

    hide = () => {
        this.update({
            visible: false,
            error: false,
            usernameError: undefined,
            passwordError: undefined,
            pendingStatus: undefined,
        })

        this.formRef.resetFields()
    }

    showConnected = (authenticated) => {
        this.update({
            authenticated,
            visible: !authenticated && this.authenticated,
        })
    }
}

export default new LoginButton()
