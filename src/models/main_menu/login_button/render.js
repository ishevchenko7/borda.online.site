import _ from 'lodash'
import React from 'react'

import { Drawer, Form, Col, Row, Input } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import { Button } from '#components'

import styles from './styles.scss'

export default function () {
    const commonStatus = this.authenticated
        ? 'success'
        : (this.pendingStatus === 'login' && 'validating') || ''

    return (
        <div className={styles.container}>
            {this.authenticated ? (
                <>
                    <div>{this.full_name}</div>
                    <Button
                        size='large'
                        icon={this.pendingStatus === 'logout' ? '' : 'logout'}
                        loading={this.pendingStatus === 'logout'}
                        onClick={this.logout}
                    />
                </>
            ) : (
                <Button size='large' icon='login' onClick={this.show} />
            )}
            <Drawer
                title='LOGIN'
                placement='right'
                visible={this.visible}
                onClose={this.hide}
            >
                <Form
                    layout='inline'
                    onFinish={this.onFinish}
                    ref={this.setFormRef}
                >
                    <Row gutter={16}>
                        <Col>
                            <Form.Item
                                name='login'
                                validateStatus={
                                    this.passwordError ? 'error' : commonStatus
                                }
                                help={this.usernameError || ''}
                            >
                                <Input
                                    prefix={
                                        <UserOutlined
                                            style={{
                                                color: 'rgba(0,0,0,.25)',
                                            }}
                                        />
                                    }
                                    autoComplete='on'
                                    placeholder='Login'
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col>
                            <Form.Item
                                name='password'
                                validateStatus={
                                    this.passwordError ? 'error' : commonStatus
                                }
                                help={this.passwordError || ''}
                            >
                                <Input
                                    prefix={
                                        <LockOutlined
                                            style={{
                                                color: 'rgba(0,0,0,.25)',
                                            }}
                                        />
                                    }
                                    type='password'
                                    autoComplete='on'
                                    placeholder='Password'
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button htmlType='submit'>OK</Button>
                        </Col>
                    </Row>
                </Form>
            </Drawer>
        </div>
    )
}
