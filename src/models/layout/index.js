import React from 'react'
import { Affix } from 'antd'

import mainMenu from '#models/main_menu'
import workspace from '#models/workspace'
import connectDialog from '#models/connect_dialog'

import styles from './styles.scss'

export default function () {
    return (
        <div className={styles.container}>
            <Affix>
                <mainMenu.View />
            </Affix>

            <div className={styles.content}>
                <workspace.View />
            </div>
            <connectDialog.View />
        </div>
    )
}
