import styles from './styles.css'

const root = document.getElementById('root')
const loader = `<div class='${styles.loader}' />`

root.insertAdjacentHTML('afterbegin', loader)
