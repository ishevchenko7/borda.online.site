import _ from 'lodash'
import pEvent from 'p-event'
import { Buffer } from 'buffer/'

import withSettings from '#common/settings-store/with_settings'

const metaLength = 24

class DataChannelWrapper {
    constructor(params) {
        _.assign(this, params)
        withSettings(this)

        this.buffers = []
        this.buffersCounter = 0
        this.log = this.log.child({ channel: this.channel.label })

        this.channel.bufferedAmountLowThreshold = this.settings.bufferedAmountLowThreshold

        this.readyEventPromise = new Promise((resolve, reject) =>
            this.setHandlers(resolve, reject)
        )
    }

    extractBuffers(data) {
        const meta = []
        const buffers = []

        function flat(value, key = '', prefix = '') {
            const path = _.join(_.filter([prefix, key]), '.')

            switch (true) {
                case Buffer.isBuffer(value) || ArrayBuffer.isView(value):
                    buffers.push(value)
                    meta.push({ path, length: value.length })

                    if (path) {
                        _.set(data, path, undefined)
                    }

                    break

                case _.isObject(value):
                    _.forEach(_.keys(value), (key) =>
                        flat(value[key], key, path)
                    )
            }
        }

        flat(data)
        return { meta, buffers }
    }

    splitBuffer(buffer, bufferId, maxLength) {
        const chunks = []
        const step = maxLength - metaLength
        const { length } = buffer

        let chunkIndex = 0
        const chunksCount = Math.ceil(length / step)
        for (let start = 0; start <= length; start += step) {
            const end = start + step >= length ? length + 1 : start + step

            const metaBuffer = Buffer.alloc(metaLength)
            metaBuffer.writeDoubleBE(bufferId, 0)
            metaBuffer.writeDoubleBE(chunkIndex, 8)
            metaBuffer.writeDoubleBE(chunksCount, 16)

            chunks.push(
                Buffer.concat([
                    metaBuffer,
                    Buffer.concat([buffer.slice(start, end)]),
                ])
            )

            chunkIndex++
        }

        return chunks
    }

    pack(obj) {
        const { meta, buffers } = this.extractBuffers(obj)

        const header = JSON.stringify({ obj, buffers: meta })
        const headerBuffer = Buffer.from(header)

        const headerLengthBuffer = Buffer.alloc(8)
        headerLengthBuffer.writeDoubleBE(headerBuffer.length, 0)

        return Buffer.concat([
            headerLengthBuffer,
            headerBuffer,
            Buffer.concat(buffers),
        ])
    }

    unpack(buffer) {
        const headerBufferLength = buffer.readDoubleBE(0)
        const header = JSON.parse(
            buffer.toString('utf8', 8, headerBufferLength + 8)
        )

        const { obj, buffers } = header

        let offset = headerBufferLength + 8
        _.forEach(buffers, ({ path, length }) => {
            _.set(obj, path, buffer.slice(offset, offset + length))
            offset += length
        })

        return obj
    }

    parseBuffer(buffer) {
        const bufferId = buffer.readDoubleBE(0)
        const chunkIndex = buffer.readDoubleBE(8)
        const chunksCount = buffer.readDoubleBE(16)
        const chunk = buffer.slice(metaLength)

        return { bufferId, chunkIndex, chunksCount, chunk }
    }

    send = async (data) => {
        const { maxMessageLength, maxBufferedAmount } = this.settings

        const buffers = this.splitBuffer(
            this.pack(data),
            this.buffersCounter++,
            maxMessageLength
        )

        for (const buffer of buffers) {
            this.channel.send(buffer)

            if (
                maxBufferedAmount > 0 &&
                this.channel.bufferedAmount > maxBufferedAmount
            ) {
                await pEvent(this.channel, 'bufferedamountlow')
            }
        }
    }

    messageHandler = ({ data }) => {
        const { bufferId, chunkIndex, chunksCount, chunk } = this.parseBuffer(
            Buffer.from(data)
        )

        if (!this.buffers[bufferId]) {
            this.buffers[bufferId] = new Array(chunksCount).fill()
        }

        this.buffers[bufferId][chunkIndex] = chunk

        if (this.buffers[bufferId].indexOf() === -1) {
            this.onMessage(this.unpack(Buffer.concat(this.buffers[bufferId])))
            delete this.buffers[bufferId]
        }
    }

    setHandlers(resolve, reject) {
        this.channel.onmessage = this.messageHandler

        this.channel.onopen = () => {
            this.log.info({ event: 'OPEN' })
            resolve(this)
        }

        this.channel.onclose = () => {
            this.log.info({ event: 'CLOSE' })
        }

        this.channel.onerror = (event) => {
            const { error } = event
            this.log.error({ event: 'ERROR', error })
            reject(error)
        }
    }
}

export default async function (params) {
    return await new DataChannelWrapper(params).readyEventPromise
}
