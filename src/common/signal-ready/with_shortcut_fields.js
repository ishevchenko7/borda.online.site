import _ from 'lodash'

export default function (destination, source, fields) {
    fields.forEach((field) => {
        if (_.isFunction(source[field])) {
            Object.defineProperty(destination, field, {
                value: (...args) => source[field](...args),
            })
        } else {
            Object.defineProperty(destination, field, {
                get: () => source[field],
            })
        }
    })
}
