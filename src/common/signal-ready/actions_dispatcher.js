import _ from 'lodash'

import logger from '#logger'
import withSettings from '#common/settings-store/with_settings'

export default class ActionsDispatcher {
    constructor(params) {
        _.assign(this, params)
        withSettings(this, 'actionsDispatcher')

        this.pendedActions = {}
        this.log = logger.child({ class: this.constructor.name })

        this.initTransceiver()
    }

    initTransceiver() {
        this.transceiver.setMessageHandler(this.messageHandler)
        this.transceiver.addEventListener('close', this.closeHandler)
    }

    recallPendedActions(dispatcher) {
        const ids = _.keys(this.pendedActions)
        _.forEach(ids, (id) => {
            const { action, resolve } = this.pendedActions[id]
            resolve({
                recallPromise: dispatcher.remoteCall(
                    _.pick(action, ['type', 'args'])
                ),
            })
        })

        return _.isEmpty(ids)
    }

    async remoteCall({ type, args }, remoteCallTimeout) {
        let timer
        const id = _.uniqueId()
        const action = { id, type, args }

        if (this.transceiver) {
            try {
                await this.transceiver.send(action)
            } catch (error) {
                this.log.error({ error })
                this.log.warn('Action will recall on next connection', action)
            }
        } else {
            throw {
                name: 'Error',
                message: 'Transceiver not initialized',
            }
        }

        let data = await new Promise((resolve) => {
            this.pendedActions[id] = { action, resolve }

            remoteCallTimeout =
                remoteCallTimeout || this.settings.remoteCallTimeout

            if (remoteCallTimeout) {
                timer = setTimeout(function () {
                    resolve({
                        error: {
                            message: `Timeout call for action ${type}`,
                        },
                    })

                    timer = undefined
                }, remoteCallTimeout)
            }
        })

        delete this.pendedActions[id]

        if (timer) {
            clearTimeout(timer)
        }

        if (data?.recallPromise) {
            data = await data.recallPromise
        }

        const { error } = data
        if (error) {
            throw {
                name: 'Remote call error',
                message:
                    error.message ||
                    (_.isString(error) ? error : JSON.stringify(error)),
            }
        }

        return data
    }

    messageHandler = async (action) => {
        if (_.isUndefined(action.result)) {
            const data = await this.call(action)

            if (this.transceiver) {
                this.transceiver.send(data)
                if (_.isFunction(this.afterSendDataHandler)) {
                    this.afterSendDataHandler(data)
                }
            } else {
                this.log.warn('Transceiver was closed')
            }
        } else {
            const pendedAction = this.pendedActions[action.id]
            if (pendedAction) {
                pendedAction.resolve(action.result)
            } else {
                this.log.warn({ action }, 'Action not in list of pended')
            }
        }
    }

    closeHandler = () => {
        delete this.transceiver
        if (this.closeTimer) {
            clearTimeout(this.closeTimer)
        }
    }

    checkPermission() {
        return true
    }

    errorHandler({ code, message }) {
        return code || message
    }

    async call(action) {
        try {
            const { type, args } = action
            const handler = this.actions(`./${type}.js`)?.default

            if (handler) {
                action.result = this.checkPermission(action)
                    ? (await handler(args, this)) || {}
                    : { error: { message: 'Permission check fail' } }
            } else {
                action.result = { error: { message: `Bad action "${type}"` } }
            }
        } catch (error) {
            this.log.error({ error })
            action.result = {
                error: { message: this.errorHandler(error) },
            }
        }

        return action
    }
}
