import _ from 'lodash'

const store = {}

function get(path) {
    const settings = path ? _.get(store, path) : store
    return settings || {}
}

function set(settings, path) {
    if (path) {
        _.set(store, path, settings)
    } else {
        _.assign(store, settings)
    }
}

function copy(from, to) {
    set(_.cloneDeep(get(from)), to)
}

function remove(key) {
    delete store[key]
}

export { get, set, copy, remove }
